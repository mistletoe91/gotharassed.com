Revolution slider.

We have created a demo slider so you have a starting point.
To import the slider:
- Go to Wordpress Admin -> Revolution Slider -> Import Slider.
- Browse to the Demo content folder, located in the package downloaded, and go to revolution_slider.
- Choose one of the .zip files located in "exported sliders" folder

Please see full Revolution Slider documentation http://themepunch.com/codecanyon/revolution_wp/documentation/