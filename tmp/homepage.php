<div class="container" id="homepagecontainer"> 
<div class="row">
  <div class="col-md-8">
      <div class="starter-template main_heading">
        <h1 class="page-header"><span> Got Harassed ?</span></h1>
        <p class="lead">Non-partisan non-profit startup reporting harassment</p>
<a href="/reports/#link_new" class="btn-red">Report Harassment Now !</a>
      </div>

   </div>   
  <div class="col-md-4">  
      <div id="homepage_active_report">
 [activity-stream pagination=0 max=20 per_page=20]
       </div>
   </div>
</div>  
</div>



<div class="row" >
  <div class="starter-template">
        <h1 class="page-header">What is this <span> all about ?</span></h1>
        <p class="lead">You will get all of the following regardless. So why don`t you sign up today ? Just do it </p>
  </div>
</div>


<div id="gt_tiles">
<div class="container">
     

    <div class="row">
        <div class="col-sm-3"> 
    		<div class="tile-progress tile-green">
    			<div class="tile-header">
    				<h3>Report</h3>
    				<span>harassments quickly</span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="30%" style="width: 30%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter"></span>Community 
    				</h4>
    				<span>and enforcement agencies based monitoring for quick action</span>
    			</div>
    		</div> 
    	</div> 


    	<div class="col-sm-3">
    		<div class="tile-progress tile-cyan">
    			<div class="tile-header">
    				<h3>Monitor</h3>
    				<span>harassment reports daily </span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="30%" style="width: 30%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter">Check</span>
    				</h4>
    				<span>respond, ask, help and alert other members</span>
    			</div>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="tile-progress tile-purple">
    			<div class="tile-header">
    				<h3>Help</h3>
    				<span>others and get help when needed</span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="30%" style="width: 30%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter"></span>Reach out

    				</h4>
    				<span>to other community members privately/publicly</span>
    			</div>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="tile-progress tile-pink">
    			<div class="tile-header">
    				<h3>Statistics </h3>
    				<span>Open data to build facts</span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="30" style="width: 30%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter"></span>Research
    				</h4>
    				<span>by tracing evidence trails and acting proactively</span>
    			</div>
    		</div>
    	</div>
    </div>
</div>
</div>

<div class="leadreportbtn">
    <a href="/reports/#link_new" class="btn-red">Report Harassment Now !</a>
</div>



<div class="row" >
  <div class="starter-template">
      <h1 class="page-header">Quick Look at <span> the facts </span></h1>
        <p class="lead">Data <small> <a target="_blank" href="http://www.unwomen.org/en/what-we-do/ending-violence-against-women/facts-and-figures">Source</a></small></p>
  </div>
</div>

<div id="gt_tiles">
<div class="container">
    
	<div class="row">
        <div class="col-sm-3">
    		<div class="tile-progress tile-primary">
    			<div class="tile-header">
    				<h3>School</h3>
    				<span>related violence</span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="90.5%" style="width: 90.5%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter">246 </span>million
    				</h4>
    				<span>An estimated 246 million boys & girls experience every year</span>
    			</div>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="tile-progress tile-red">
    			<div class="tile-header">
    				<h3>Killed</h3>
    				<span>by intimate partners</span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="50%" style="width: 50%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter">50</span>% 
    				</h4>
    				<span>all women who were the victims of homicide globally in 2012</span>
    			</div>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="tile-progress tile-blue">
    			<div class="tile-header">
    				<h3>Married </h3>
    				<span>before their 18th birthday</span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="95%" style="width: 95%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter">750 </span>million worldwide
    				</h4>
    				<span>In West/Central Africa, over 4 in 10 girls were married before 18</span>
    			</div>
    		</div>
    	</div>
    	<div class="col-sm-3">
    		<div class="tile-progress tile-aqua">
    			<div class="tile-header">
    				<h3>Undergone </h3>
    				<span>female genital mutilation</span>
    			</div>
    			<div class="tile-progressbar">
    				<span data-fill="22%" style="width: 22%;"></span>
    			</div>
    			<div class="tile-footer">
    				<h4>
    					<span class="pct-counter">200</span> million women
    				</h4>
    				<span>in the 30 countries with representative data on prevalence</span>
    			</div>
    		</div>
    	</div>
    </div>
    
    
 
</div>
</div>

<div class="leadreportbtn">
    <a href="/reports/#link_new" class="btn-red">Report Harassment Now !</a>
</div>


<!-- Icons -->
      
 <!-- Portfolio -->
<div class="row" >
  <div class="starter-template">
        <h1 class="page-header">Why Chose Our   <span>Cakes ?</span></h1>
        <p class="lead">You will get all of the following regardless. So why don`t you sign up today ? Just do it </p>
  </div>

    <div class="row stylish-panel">
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-cloud  insideicons"  ></i>
             </div>
           </div>
           <h2>While-label Service</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-television  insideicons" ></i>
             </div>
           </div>
           <h2>Easy-Embedd Widget</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-mobile-phone insideicons" ></i>
             </div>
           </div>
           <h2>Mobile App</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
    </div>

    <div class="row stylish-panel">
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-shield insideicons"  ></i>
             </div>
           </div>
           <h2>Robust Security</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management .
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-hand-o-up insideicons" ></i>
             </div>
           </div>
           <h2>Control Document Transfer</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div> 
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-thumb-tack insideicons" ></i>
             </div>
           </div>
           <h2>Tracking</h2>
           <p class="moreinfotext">TrThis service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
    </div>
    <div style="Clear:both"></div>
</div>
<div style="Clear:both"></div>
<!-- /Portfolio -->
