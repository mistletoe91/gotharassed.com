<?php
/**
 * BuddyPress Activity templates
 *
 * @since 2.3.0
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/**
 * Fires before the activity directory listing.
 *
 * @since 1.5.0
 */
do_action( 'bp_before_directory_activity' ); ?>

<div class="container"> 
    <div class="row">
 
<div id="buddypress">
    
    
	<?php
 
	do_action( 'bp_before_directory_activity_content' ); ?>
    
	<div id="template-notices" role="alert" aria-atomic="true">
		<?php

		/**
		 * Fires towards the top of template pages for notice display.
		 *
		 * @since 1.0.0
		 */
		do_action( 'template_notices' ); ?>

	</div>    
    

	<div class="item-list-tabs activity-type-tabs" aria-label="<?php esc_attr_e( 'Sitewide activities navigation', 'buddypress' ); ?>" role="navigation">
		<ul>
			<?php

			/**
			 * Fires before the listing of activity type tabs.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_before_activity_type_tab_all' ); ?>

			<li class="selected" id="activity-all"><a href="<?php bp_activity_directory_permalink(); ?>"><?php printf( __( 'All Members %s', 'buddypress' ), '<span>' . bp_get_total_member_count() . '</span>' ); ?></a></li>

			<?php if ( is_user_logged_in() ) : ?>

				<?php

				/**
				 * Fires before the listing of friends activity type tab.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_before_activity_type_tab_friends' ); ?>

				<?php if ( bp_is_active( 'friends' ) ) : ?>

					<?php if ( bp_get_total_friend_count( bp_loggedin_user_id() ) ) : ?>

						<li id="activity-friends"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/' . bp_get_friends_slug() . '/'; ?>"><?php printf( __( 'My Friends %s', 'buddypress' ), '<span>' . bp_get_total_friend_count( bp_loggedin_user_id() ) . '</span>' ); ?></a></li>

					<?php endif; ?>

				<?php endif; ?>

				<?php

				/**
				 * Fires before the listing of groups activity type tab.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_before_activity_type_tab_groups' ); ?>

				<?php if ( bp_is_active( 'groups' ) ) : ?>

					<?php if ( bp_get_total_group_count_for_user( bp_loggedin_user_id() ) ) : ?>

						<li id="activity-groups"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/' . bp_get_groups_slug() . '/'; ?>"><?php printf( __( 'My Groups %s', 'buddypress' ), '<span>' . bp_get_total_group_count_for_user( bp_loggedin_user_id() ) . '</span>' ); ?></a></li>

					<?php endif; ?>

				<?php endif; ?>

				<?php

				/**
				 * Fires before the listing of favorites activity type tab.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_before_activity_type_tab_favorites' ); ?>

				<?php if ( bp_get_total_favorite_count_for_user( bp_loggedin_user_id() ) ) : ?>

					<li id="activity-favorites"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/favorites/'; ?>"><?php printf( __( 'My Favorites %s', 'buddypress' ), '<span>' . bp_get_total_favorite_count_for_user( bp_loggedin_user_id() ) . '</span>' ); ?></a></li>

				<?php endif; ?>

				<?php if ( bp_activity_do_mentions() ) : ?>

					<?php

					/**
					 * Fires before the listing of mentions activity type tab.
					 *
					 * @since 1.2.0
					 */
					do_action( 'bp_before_activity_type_tab_mentions' ); ?>

					<li id="activity-mentions"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/mentions/'; ?>"><?php _e( 'Mentions', 'buddypress' ); ?><?php if ( bp_get_total_mention_count_for_user( bp_loggedin_user_id() ) ) : ?> <strong><span><?php printf( _nx( '%s new', '%s new', bp_get_total_mention_count_for_user( bp_loggedin_user_id() ), 'Number of new activity mentions', 'buddypress' ), bp_get_total_mention_count_for_user( bp_loggedin_user_id() ) ); ?></span></strong><?php endif; ?></a></li>

				<?php endif; ?>

			<?php endif; ?>

			<?php

			/**
			 * Fires after the listing of activity type tabs.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_activity_type_tabs' ); ?>
		</ul>
	</div><!-- .item-list-tabs -->

	<div class="item-list-tabs no-ajax" id="subnav" aria-label="<?php esc_attr_e( 'Activity secondary navigation', 'buddypress' ); ?>" role="navigation">
		<ul>
			<li class="feed"><a href="<?php bp_sitewide_activity_feed_link(); ?>" class="bp-tooltip" data-bp-tooltip="<?php esc_attr_e( 'RSS Feed', 'buddypress' ); ?>" aria-label="<?php esc_attr_e( 'RSS Feed', 'buddypress' ); ?>"><?php _e( 'RSS', 'buddypress' ); ?></a></li>

			<?php

			/**
			 * Fires before the display of the activity syndication options.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_activity_syndication_options' ); ?>

			<li id="activity-filter-select" class="last">
				<label for="activity-filter-by"><?php _e( 'Show:', 'buddypress' ); ?></label>
				<select id="activity-filter-by">
					<option value="-1"><?php _e( '&mdash; Everything &mdash;', 'buddypress' ); ?></option>

					<?php bp_activity_show_filters(); ?>

					<?php

					/**
					 * Fires inside the select input for activity filter by options.
					 *
					 * @since 1.2.0
					 */
					do_action( 'bp_activity_filter_options' ); ?>

				</select>
			</li>
		</ul>
	</div><!-- .item-list-tabs -->

    
      <!-- FORM START -->  
    <div class="page bg-white"> 
      <!-- Forum Content -->
      <div class="page-main">

        <!-- Forum Content Header -->
        <div class="page-header">
          <h1 class="page-title">Getting Started</h1>
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon wb-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>

        <!-- Forum Nav -->
        <div class="page-nav-tabs">
          <ul class="nav nav-tabs nav-tabs-line" role="tablist">
            <li class="nav-item" role="presentation">
              <a class="active nav-link" data-toggle="tab" href="#forum-allreports" aria-controls="forum-allreports"
                aria-expanded="true" role="tab">All Reports</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link" data-toggle="tab" href="#forum-newreport" aria-controls="forum-newreport"
                aria-expanded="false" role="tab">Report Harassement</a>
            </li> 
          </ul>
        </div>

        <!-- Forum Content -->
        <div class="page-content tab-content page-content-table nav-tabs-animate">
          <div class="tab-pane animation-fade active" id="forum-allreports" role="tabpanel">
              
	<?php

	/**
	 * Fires before the display of the activity list.
	 *
	 * @since 1.5.0
	 */
	do_action( 'bp_before_directory_activity_list' ); ?>

	<div class="activity" aria-live="polite" aria-atomic="true" aria-relevant="all">

		<?php bp_get_template_part( 'activity/activity-loop' ); ?>

	</div><!-- .activity -->

	<?php

	/**
	 * Fires after the display of the activity list.
	 *
	 * @since 1.5.0
	 */
	do_action( 'bp_after_directory_activity_list' ); ?>

	<?php

	/**
	 * Fires inside and displays the activity directory display content.
	 */
	do_action( 'bp_directory_activity_content' ); ?>

	<?php

	/**
	 * Fires after the activity directory display content.
	 *
	 * @since 1.2.0
	 */
	do_action( 'bp_after_directory_activity_content' ); ?>

	<?php

	/**
	 * Fires after the activity directory listing.
	 *
	 * @since 1.5.0
	 */
	do_action( 'bp_after_directory_activity' ); ?>
              
            <table class="table is-indent">
              <tbody>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/1.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Vicinum at aperta, torquem mox doloris illi, officiis.
                        <div class="flags responsive-hide">
                          <span class="badge badge-round badge-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                          <i class="locked icon wb-lock" aria-hidden="true"></i>
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Herman Beck</span>
                        <span class="started">1 day ago</span>
                        <span class="tags">Themes</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">1</span>
                    <span class="unit">Post</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/2.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Moribus ibidem angore, iudiciorumque careret causa verbis aliena.
                        <div class="flags responsive-hide">
                          <i class="locked icon wb-lock" aria-hidden="true"></i>
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Mary Adams</span>
                        <span class="started">2 days ago</span>
                        <span class="tags">Configuration</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">2</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/3.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Sinat ut miseram voluptatibus compositis quodsi. Quem afflueret.
                        <div class="flags responsive-hide">
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Caleb Richards</span>
                        <span class="started">3 days ago</span>
                        <span class="tags">Installation</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">3</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/4.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Graeca modice video patre iuste tradidisse molestiae molestia.
                        <div class="flags responsive-hide">
                          <i class="locked icon wb-lock" aria-hidden="true"></i>
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By June Lane</span>
                        <span class="started">4 days ago</span>
                        <span class="tags">Announcements</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">4</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/5.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Autem omnes is protervi fortitudinis maerores, geometrica statuat.
                        <div class="flags responsive-hide">
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Edward Fletcher</span>
                        <span class="started">5 days ago</span>
                        <span class="tags">Development</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">5</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/6.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Tuemur geometrica angore haeret rogatiuncula albuci meo etiam.
                        <div class="flags responsive-hide">
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Crystal Bates</span>
                        <span class="started">6 days ago</span>
                        <span class="tags">Plugins</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">6</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/7.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Caret adoptionem tollitur, agam dixeris respondendum fortunae familias.
                        <div class="flags responsive-hide">
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Nathan Watts</span>
                        <span class="started">7 days ago</span>
                        <span class="tags">Technical Support</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">7</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/8.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Una veniamus fruentem firmam, explicari laboramus futuris miser.
                        <div class="flags responsive-hide">
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Heather Harper</span>
                        <span class="started">8 days ago</span>
                        <span class="tags">Code Review</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">8</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/9.jpg" alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Aristippus dicantur verterem molestiam tali appetendum. Maximis potest.
                        <div class="flags responsive-hide">
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Willard Wood</span>
                        <span class="started">9 days ago</span>
                        <span class="tags">Responses</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">9</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
                <tr data-url="panel.tpl" data-toggle="slidePanel">
                  <td class="pre-cell"></td>
                  <td class="cell-60 responsive-hide">
                    <a class="avatar" href="javascript:void(0)">
                      <img class="img-fluid" src="../../../../global/portraits/10.jpg"
                        alt="...">
                    </a>
                  </td>
                  <td>
                    <div class="content">
                      <div class="title">
                        Hac ipsa sit, facile liberiusque ipse frustra multo.
                        <div class="flags responsive-hide">
                        </div>
                      </div>
                      <div class="metas">
                        <span class="author">By Ronnie Ellis</span>
                        <span class="started">10 days ago</span>
                        <span class="tags">Package</span>
                      </div>
                    </div>
                  </td>
                  <td class="cell-80 forum-posts">
                    <span class="num">10</span>
                    <span class="unit">Posts</span>
                  </td>
                  <td class="suf-cell"></td>
                </tr>
              </tbody>
            </table>
             
          </div>
          <div class="tab-pane animation-fade" id="forum-newreport" role="tabpanel"> 
             
                <?php if ( is_user_logged_in() ) : ?>

                    <?php bp_get_template_part( 'activity/post-form' ); ?>

                <?php endif; ?>              
              
          </div> 
        </div>
      </div>
    </div>
    
      <!-- FORM END -->
    
    







    


</div>
        
         
    </div>
</div>

  
