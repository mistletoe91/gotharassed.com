<?php
/**
 * BuddyPress - Activity Loop
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/**
 * Fires before the start of the activity loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_activity_loop' ); ?>

<?php if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) ) : ?>



	<?php if ( empty( $_POST['page'] ) ) : ?>

    <div id="recentActivityWidget" class="card card-shadow card-lg pb-20">                   
        <div class="page-content">
         <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12">

                <div class="example-wrap">
                  <h4 class="example-title">Basic Form (Form grid)</h4>
                    
                    <div class="alert alert-primary" role="alert">
                      This is a primary alert—check it out! This is a primary alert—check it out! This is a primary alert—check it out! This is a primary alert—check it out! This is a primary alert—check it out!
                    </div>
                    
            <div id="recentActivityWidget" class="card card-shadow card-lg pb-20">
                
              <div class="card-header card-header-transparent pb-10">
                <div class="card-header-actions hide">
                  <span class="badge badge-default badge-round">VIEW ALL</span>
                </div>
                <h5 class="card-title">
                  HARASSEMENT REPORTS
                </h5>
              </div>                 
                
		<ul id="activity-stream" class="timeline timeline-icon"> 

	<?php endif; ?>
    

            
	<?php while ( bp_activities() ) : bp_the_activity(); ?>

		<?php bp_get_template_part( 'activity/entry' ); ?>

	<?php endwhile; ?>

	<?php if ( bp_activity_has_more_items() ) : ?>

		<li class="load-more">
			<a href="<?php bp_activity_load_more_link() ?>"><?php _e( 'Load More', 'buddypress' ); ?></a>
		</li>

	<?php endif; ?>

	<?php if ( empty( $_POST['page'] ) ) : ?>

		</ul>
            
                </div>
              </div>
            </div>
          </div>
        </div>
         </div>
        </div>
    </div>
        
	<?php endif; ?>

<?php else : ?>

	<div id="message" class="info">
		<p><?php _e( 'Sorry, there was no activity found. Please try a different filter.', 'buddypress' ); ?></p>
	</div>

<?php endif; ?>

<?php

/**
 * Fires after the finish of the activity loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_activity_loop' ); ?>

<?php if ( empty( $_POST['page'] ) ) : ?>

	<form action="" name="activity-loop-form" id="activity-loop-form" method="post">

		<?php wp_nonce_field( 'activity_filter', '_wpnonce_activity_filter' ); ?>

	</form>

<?php endif;
