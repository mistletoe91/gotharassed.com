<?php
/**
 * BuddyPress - Activity Post Form
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */
 
$optionsArray = json_decode(CUSTOM_OPTIONS, true);
 
 
                      
function displayOptions ( $optionsArray, $optionName)                      
{
    $return = '';
    foreach($optionsArray [$optionName] as $i=>$v ){
        
        if($i === 'optgroup'){
             foreach($v  as $ii=>$vv ){
              $return .= '<optgroup label="'.$vv ['label'].'">';
                foreach($vv ['options'] as $iii=>$vvv ){
                 $return .= '<option value="'.$iii.'">'.$vvv ['Name'].'</option>';
                }//end foreach
             }//end foreach
            $return .= "</optgroup>";
        } else {
            $selected_ = '';
            if(isset($v ['Selected']) && $v ['Selected']){
                $selected_ = 'selected';
            }//endif
            $return .= '<option value="'.$i.'" '.$selected_.'>'.$v ['Name'].'</option>';
        }//endif        

    }//end foreach
    return $return;
}

?> 



<div id="recentActivityWidget" class="card card-shadow card-lg pb-20">                   
    <div class="page-content">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12">
                  

                  
                <!-- Example Basic Form (Form grid) -->
                <div class="example-wrap">
                  <h4 class="example-title">Basic Form (Form grid)</h4>
                    
                    <div class="alert alert-primary" role="alert">
                      This is a primary alert—check it out! This is a primary alert—check it out! This is a primary alert—check it out! This is a primary alert—check it out! This is a primary alert—check it out!
                    </div>
                    
<form action="<?php bp_activity_post_form_action(); ?>" method="post" id="whats-new-form" name="whats-new-form">
    
                  <div class="example">  
                      

                      <!-- GEO LOCATION START -->
     
<table id="address" class="hide">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number" name="street_number"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route" name="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <!-- Note: Selection of address components in this example is typical.
             You may need to adjust it for the locations relevant to your app. See
             https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
        -->
        <td class="wideField" colspan="3"><input class="field" id="locality" name="locality"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" name="administrative_area_level_1" disabled="true"></input></td>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" id="postal_code" name="postal_code"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field" name="country"
              id="country" disabled="true"></input></td>
      </tr>
    </table>

    
                      <!-- GEO LOCATION END   -->
                      
                    
                      
                      
                      <div class="form-group">
                        <label class="form-control-label" for="ctlsev">Severity</label>                       
               
                    <div class="input-group"> 
                     <select class="form-control" id="ctlsev"  name="ctlsev" data-plugin="select2">  
                        <?php echo displayOptions ( $optionsArray, 'ctlsev'); ?> 
                    </select> 
                    </div>
                       </div>      


                      <div class="form-group">
                        <label class="form-control-label" for="whats-new">Description Of Incident</label>  
		<div id="whats-new-textarea">
<textarea cols="200" class="bp-suggestions form-control" name="whats-new" id="whats-new" cols="50" rows="10"
				<?php if ( bp_is_group() ) : ?>data-suggestions-group-id="<?php echo esc_attr( (int) bp_get_current_group_id() ); ?>" <?php endif; ?>
			><?php if ( isset( $_GET['r'] ) ) : ?>@<?php echo esc_textarea( $_GET['r'] ); ?> <?php endif; ?></textarea>  
            <span class="text-help">Please provide as much detail as possible </span>
			
		</div>
                          

	<div id="whats-new-content">


		<div id="whats-new-options">


			<?php if ( bp_is_active( 'groups' ) && !bp_is_my_profile() && !bp_is_group() ) : ?>

				<div id="whats-new-post-in-box">

					<?php _e( 'Post in', 'buddypress' ); ?>:

					<label for="whats-new-post-in" class="bp-screen-reader-text"><?php
						/* translators: accessibility text */
						_e( 'Post in', 'buddypress' );
					?></label>
					<select id="whats-new-post-in" name="whats-new-post-in">
						<option selected="selected" value="0"><?php _e( 'My Profile', 'buddypress' ); ?></option>

						<?php if ( bp_has_groups( 'user_id=' . bp_loggedin_user_id() . '&type=alphabetical&max=100&per_page=100&populate_extras=0&update_meta_cache=0' ) ) :
							while ( bp_groups() ) : bp_the_group(); ?>

								<option value="<?php bp_group_id(); ?>"><?php bp_group_name(); ?></option>

							<?php endwhile;
						endif; ?>

					</select>
				</div>
				<input type="hidden" id="whats-new-post-object" name="whats-new-post-object" value="groups" />

			<?php elseif ( bp_is_group_activity() ) : ?>

				<input type="hidden" id="whats-new-post-object" name="whats-new-post-object" value="groups" />
				<input type="hidden" id="whats-new-post-in" name="whats-new-post-in" value="<?php bp_group_id(); ?>" />

			<?php endif; ?>

			<?php

			/**
			 * Fires at the end of the activity post form markup.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_activity_post_form_options' ); ?>

		</div><!-- #whats-new-options -->
	</div><!-- #whats-new-content -->

                          
                          
	<?php wp_nonce_field( 'post_update', '_wpnonce_post_update' ); ?>
	<?php 
	do_action( 'bp_after_activity_post_form' ); ?>                          
                                                    
                      </div>                      
                       
                      
                       <div class="form-group">
                        <label class="form-control-label" for="ctltype">Type of Harrasement</label> 
                                             
                    <select class="form-control" id="ctltype" name="ctltype[]" multiple data-plugin="select2"> 
                        <?php echo displayOptions ( $optionsArray, 'ctltype'); ?> 
                    </select>
      
                           </div>



  
                      <div class="form-group">
                        <label class="form-control-label" for="inputBasicEmail">Incident Location</label> 
                           

        <input type="text" name="txtincident_location"  onFocus="geolocate()"   class="form-control" placeholder="Start Typing Address"  id="txtincident_location" value="" /> 
                      </div>
                      
                      

                      
                       <div class="form-group">
                        <label class="form-control-label"  >Date & Time of Incident</label>                       
                 <div class="input-daterange-wrap">
                  <div class="input-daterange">
                    <div class="input-group">
                      <span class="input-group-addon">
                      a  <i class="icon wb-calendar" aria-hidden="true"></i>
                      </span>
                      <input type="text"  value="<?php echo date("m-d-Y");?>" name="ctldate" id="ctldate" class="form-control datepair-date datepair-start" data-plugin="datepicker">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon wb-time" aria-hidden="true"></i>
                      </span>
                        
                        
<?php
                                      
                        
    /* This sets the $time variable to the current hour in the 24 hour clock format */
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
    $timezone = date("e");
    /* If the time is less than 1200 hours, show good morning */
    if ($time < "12") {        
        $optionsArray ['ctltime'][10]['Selected'] = true;
    } else
    /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
    if ($time >= "12" && $time < "17") {
        $optionsArray ['ctltime'][2]['Selected'] = true;
    } else
    /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
    if ($time >= "17" && $time < "19") {
        $optionsArray ['ctltime'][3]['Selected'] = true;
    } else
    /* Finally, show good night if the time is greater than or equal to 1900 hours */
    if ($time >= "19") {
        $optionsArray ['ctltime'][4]['Selected'] = true;
    }
    ?>
                        
                     <select class="form-control"  name="ctltime" id="ctltime" data-plugin="select2"> 
                        <?php echo displayOptions ( $optionsArray, 'ctltime'); ?>   
                    </select>
                        
                        
                    </div>
                  </div>
                </div>
                       </div>
                      
                      
                       
                           <div class="form-group">
                        <label class="form-control-label" for="ctlfactors_physical">Factors</label>  
                    <div class="input-group"> 
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="ctlfactors_physical" name="ctlfactors_physical[]" value="1">
                          <label for="ctlfactors_physical">Involve physical Harrasment</label> 
                        </div> 
                    </div> 
                    <div class="input-group">
                        <div class="checkbox-custom checkbox-primary">
                          <input type="checkbox" id="ctlfactors_more_than_one_victum" name="ctlfactors_more_than_one_victum[]" value="1"> 
                          <label for="ctlfactors_more_than_one_victum">More than One victum</label>
                        </div>                         
                    </div>
                       </div>                        
                      
          
   
                           <div class="panel panel-primary panel-line">
                      <div class="panel-heading">
                        <h3 class="panel-title">Offender`s Profile</h3>
                      </div>
                      <div class="panel-body">Please use below form to fill down offender`s profile. In case of multiple offender, please fill the primary offender`s profile and other related information </div>
                    </div>                       
                      
                                  <!-- Offender`s Identity Start -->    
                      
                           <div class="form-group">
                            <label class="form-control-label" for="ctlnumberofoffenders">Number of Offenders</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctlnumberofoffenders" name="ctlnumberofoffenders"  data-plugin="select2">  
                                    <?php echo displayOptions ( $optionsArray, 'ctlnumberofoffenders'); ?>   
                                </select> 
                                </div>
                           </div>  
                      
                           <div class="form-group">
                            <label class="form-control-label" for="ctltypeofoffender">Type of Offender</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctltypeofoffender" name="ctltypeofoffender"  data-plugin="select2">
                                    <?php echo displayOptions ( $optionsArray, 'ctltypeofoffender'); ?>
                                </select> 
                                </div>
                           </div>                      
                        
                           <div class="form-group">
                            <label class="form-control-label" for="ctlgender">Offender`s Gender</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctlgender" name="ctlgender"  data-plugin="select2">  
                                     <?php echo displayOptions ( $optionsArray, 'ctlgender'); ?>
 
                                </select> 
                                </div>
                           </div> 

                           <div class="form-group">
                            <label class="form-control-label" for="ctlagegroup">Offender`s Age Group</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctlagegroup" name="ctlagegroup"  data-plugin="select2">  
                                     <?php echo displayOptions ( $optionsArray, 'ctlagegroup'); ?>
 
                                </select> 
                                </div>
                           </div> 
                            
                            
                           <div class="form-group">
                            <label class="form-control-label" for="ctlrace">Offender`s Ethnicity </label>   
                                <div class="input-group">  
                                     
<select class="form-control dropdown" id="ctlrace" name="ctlrace"  data-plugin="select2">
    <option value="" selected="selected" disabled="disabled">-- select one --</option>
    <optgroup label="White">
      <option value="White English">English</option>
      <option value="White Welsh">Welsh</option>
      <option value="White Scottish">Scottish</option>
      <option value="White Northern Irish">Northern Irish</option>
      <option value="White Irish">Irish</option>
      <option value="White Gypsy or Irish Traveller">Gypsy or Irish Traveller</option>
      <option value="White Other">Any other White background</option>
    </optgroup>
    <optgroup label="Mixed or Multiple ethnic groups">
      <option value="Mixed White and Black Caribbean">White and Black Caribbean</option>
      <option value="Mixed White and Black African">White and Black African</option>
      <option value="Mixed White Other">Any other Mixed or Multiple background</option>
    </optgroup>
    <optgroup label="Asian">
      <option value="Asian Indian">Indian</option>
      <option value="Asian Pakistani">Pakistani</option>
      <option value="Asian Bangladeshi">Bangladeshi</option>
      <option value="Asian Chinese">Chinese</option>
      <option value="Thai, Malay, Filipino">Thai, Malay, Filipino</option>   
      <option value="North East Asian">North East Asian (Mongol, Tibetan, Korean Japanese, etc)</option>           
      <option value="Asian Other">Any other Asian background</option>
    </optgroup>
    <optgroup label="Black">
      <option value="Black African">African</option>
      <option value="Black African American">African American</option>
      <option value="Black Caribbean">Caribbean</option>
      <option value="Black Other">Any other Black background</option>
    </optgroup>
    <optgroup label="Other ethnic groups">      
      <option value="Hispanic">Hispanic</option>
      <option value="Latino">Latino</option>
      <option value="Native American">Native American</option>
      <option value="Indigenous Australian">Indigenous Australian</option>        
      <option value="Pacific Islander">Pacific Islander</option>
      <option value="Pacific">Pacific (Polynesian, Micronesian, etc)</option>
      <option value="Arctic">Arctic (Siberian, Eskimo)</option>
      <option value="Arab">Arab</option>
      <option value="Other">Any other ethnic group</option>
    </optgroup>
  </select>                                    
                                                                         
                                </div>
                           </div>   
                      

                           <div class="form-group">
                            <label class="form-control-label" for="ctlprimarylang">Offender`s Primary Language (If Known)</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctlprimarylang" name="ctlprimarylang"  data-plugin="select2"> 
  <option value="AF">Afrikanns</option>
  <option value="SQ">Albanian</option>
  <option value="AR">Arabic</option>
  <option value="HY">Armenian</option>
  <option value="EU">Basque</option>
  <option value="BN">Bengali</option>
  <option value="BG">Bulgarian</option>
  <option value="CA">Catalan</option>
  <option value="KM">Cambodian</option>
  <option value="ZH">Chinese (Mandarin)</option>
  <option value="HR">Croation</option>
  <option value="CS">Czech</option>
  <option value="DA">Danish</option>
  <option value="NL">Dutch</option>
  <option value="EN">English</option>
  <option value="ET">Estonian</option>
  <option value="FJ">Fiji</option>
  <option value="FI">Finnish</option>
  <option value="FR">French</option>
  <option value="KA">Georgian</option>
  <option value="DE">German</option>
  <option value="EL">Greek</option>
  <option value="GU">Gujarati</option>
  <option value="HE">Hebrew</option>
  <option value="HI">Hindi</option>
  <option value="HU">Hungarian</option>
  <option value="IS">Icelandic</option>
  <option value="ID">Indonesian</option>
  <option value="GA">Irish</option>
  <option value="IT">Italian</option>
  <option value="JA">Japanese</option>
  <option value="JW">Javanese</option>
  <option value="KO">Korean</option>
  <option value="LA">Latin</option>
  <option value="LV">Latvian</option>
  <option value="LT">Lithuanian</option>
  <option value="MK">Macedonian</option>
  <option value="MS">Malay</option>
  <option value="ML">Malayalam</option>
  <option value="MT">Maltese</option>
  <option value="MI">Maori</option>
  <option value="MR">Marathi</option>
  <option value="MN">Mongolian</option>
  <option value="NE">Nepali</option>
  <option value="NO">Norwegian</option>
  <option value="FA">Persian</option>
  <option value="PL">Polish</option>
  <option value="PT">Portuguese</option>
  <option value="PA">Punjabi</option>
  <option value="QU">Quechua</option>
  <option value="RO">Romanian</option>
  <option value="RU">Russian</option>
  <option value="SM">Samoan</option>
  <option value="SR">Serbian</option>
  <option value="SK">Slovak</option>
  <option value="SL">Slovenian</option>
  <option value="ES">Spanish</option>
  <option value="SW">Swahili</option>
  <option value="SV">Swedish </option>
  <option value="TA">Tamil</option>
  <option value="TT">Tatar</option>
  <option value="TE">Telugu</option>
  <option value="TH">Thai</option>
  <option value="BO">Tibetan</option>
  <option value="TO">Tonga</option>
  <option value="TR">Turkish</option>
  <option value="UK">Ukranian</option>
  <option value="UR">Urdu</option>
  <option value="UZ">Uzbek</option>
  <option value="VI">Vietnamese</option>
  <option value="CY">Welsh</option>
  <option value="XH">Xhosa</option>
</select>                       
                                </div>
                           </div>                       
                                  <!-- Offender`s Identity Stop -->                      

                      
                           <div class="panel panel-primary panel-line">
                      <div class="panel-heading">
                        <h3 class="panel-title">Optional Voluntary Disclosure</h3>
                      </div>
                      <div class="panel-body">Information below is optional and does not get displayed along with your record. We highly encourage to fill this information it is used as an aggregate for statistical purposes. 
                      </div>    
                      
              <!-- Example Default Accordion -->   
                  <div class="panel-group" id="exampleAccordionDefault" aria-multiselectable="true"
                    role="tablist">                       
                      
                    <div class="panel">            
                      <div class="panel-heading" id="exampleHeadingDefaultTwo" role="tab">
                        <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultTwo"
                          data-parent="#exampleAccordionDefault" aria-expanded="false"
                          aria-controls="exampleCollapseDefaultTwo">
                      Your Profile
                    </a>
                      </div>
                      <div class="panel-collapse collapse" id="exampleCollapseDefaultTwo" aria-labelledby="exampleHeadingDefaultTwo"
                        role="tabpanel">
                        <div class="panel-body">
                             
                            
                           <div class="form-group">
                            <label class="form-control-label" for="ctlgender_you">Your Gender</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctlgender_you" name="ctlgender_you"  data-plugin="select2">  
                                     <?php echo displayOptions ( $optionsArray, 'ctlgender'); ?>
 
                                </select> 
                                </div>
                           </div>                             
                            
                           <div class="form-group">
                            <label class="form-control-label" for="ctlagegroup_you">Your Age Group</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctlagegroup_you" name="ctlagegroup_you"  data-plugin="select2"> 
                                      <?php echo displayOptions ( $optionsArray, 'ctlagegroup'); ?> 
                                </select> 
                                </div>
                           </div> 
                            
                           <div class="form-group">
                            <label class="form-control-label" for="ctlprimarylang_you">Your Primary Language</label>   
                                <div class="input-group"> 
                                 <select class="form-control" id="ctlprimarylang_you" name="ctlprimarylang_you"  data-plugin="select2"> 
  <option value="AF">Afrikanns</option>
  <option value="SQ">Albanian</option>
  <option value="AR">Arabic</option>
  <option value="HY">Armenian</option>
  <option value="EU">Basque</option>
  <option value="BN">Bengali</option>
  <option value="BG">Bulgarian</option>
  <option value="CA">Catalan</option>
  <option value="KM">Cambodian</option>
  <option value="ZH">Chinese (Mandarin)</option>
  <option value="HR">Croation</option>
  <option value="CS">Czech</option>
  <option value="DA">Danish</option>
  <option value="NL">Dutch</option>
  <option value="EN">English</option>
  <option value="ET">Estonian</option>
  <option value="FJ">Fiji</option>
  <option value="FI">Finnish</option>
  <option value="FR">French</option>
  <option value="KA">Georgian</option>
  <option value="DE">German</option>
  <option value="EL">Greek</option>
  <option value="GU">Gujarati</option>
  <option value="HE">Hebrew</option>
  <option value="HI">Hindi</option>
  <option value="HU">Hungarian</option>
  <option value="IS">Icelandic</option>
  <option value="ID">Indonesian</option>
  <option value="GA">Irish</option>
  <option value="IT">Italian</option>
  <option value="JA">Japanese</option>
  <option value="JW">Javanese</option>
  <option value="KO">Korean</option>
  <option value="LA">Latin</option>
  <option value="LV">Latvian</option>
  <option value="LT">Lithuanian</option>
  <option value="MK">Macedonian</option>
  <option value="MS">Malay</option>
  <option value="ML">Malayalam</option>
  <option value="MT">Maltese</option>
  <option value="MI">Maori</option>
  <option value="MR">Marathi</option>
  <option value="MN">Mongolian</option>
  <option value="NE">Nepali</option>
  <option value="NO">Norwegian</option>
  <option value="FA">Persian</option>
  <option value="PL">Polish</option>
  <option value="PT">Portuguese</option>
  <option value="PA">Punjabi</option>
  <option value="QU">Quechua</option>
  <option value="RO">Romanian</option>
  <option value="RU">Russian</option>
  <option value="SM">Samoan</option>
  <option value="SR">Serbian</option>
  <option value="SK">Slovak</option>
  <option value="SL">Slovenian</option>
  <option value="ES">Spanish</option>
  <option value="SW">Swahili</option>
  <option value="SV">Swedish </option>
  <option value="TA">Tamil</option>
  <option value="TT">Tatar</option>
  <option value="TE">Telugu</option>
  <option value="TH">Thai</option>
  <option value="BO">Tibetan</option>
  <option value="TO">Tonga</option>
  <option value="TR">Turkish</option>
  <option value="UK">Ukranian</option>
  <option value="UR">Urdu</option>
  <option value="UZ">Uzbek</option>
  <option value="VI">Vietnamese</option>
  <option value="CY">Welsh</option>
  <option value="XH">Xhosa</option>
</select>                       
                                </div>
                           </div> 
                            
                           <div class="form-group">
                            <label class="form-control-label" for="ctlrace_you">Your Ethnicity</label>   
                                <div class="input-group">  
                                     
<select class="form-control dropdown" id="ctlrace_you" name="ctlrace_you"  data-plugin="select2">
    <option value="" selected="selected" disabled="disabled">-- select one --</option>
    <optgroup label="White">
      <option value="White English">English</option>
      <option value="White Welsh">Welsh</option>
      <option value="White Scottish">Scottish</option>
      <option value="White Northern Irish">Northern Irish</option>
      <option value="White Irish">Irish</option>
      <option value="White Gypsy or Irish Traveller">Gypsy or Irish Traveller</option>
      <option value="White Other">Any other White background</option>
    </optgroup>
    <optgroup label="Mixed or Multiple ethnic groups">
      <option value="Mixed White and Black Caribbean">White and Black Caribbean</option>
      <option value="Mixed White and Black African">White and Black African</option>
      <option value="Mixed White Other">Any other Mixed or Multiple background</option>
    </optgroup>
    <optgroup label="Asian">
      <option value="Asian Indian">Indian</option>
      <option value="Asian Pakistani">Pakistani</option>
      <option value="Asian Bangladeshi">Bangladeshi</option>
      <option value="Asian Chinese">Chinese</option>
      <option value="Thai, Malay, Filipino">Thai, Malay, Filipino</option>   
      <option value="North East Asian">North East Asian (Mongol, Tibetan, Korean Japanese, etc)</option>           
      <option value="Asian Other">Any other Asian background</option>
    </optgroup>
    <optgroup label="Black">
      <option value="Black African">African</option>
      <option value="Black African American">African American</option>
      <option value="Black Caribbean">Caribbean</option>
      <option value="Black Other">Any other Black background</option>
    </optgroup>
    <optgroup label="Other ethnic groups">      
      <option value="Hispanic">Hispanic</option>
      <option value="Latino">Latino</option>
      <option value="Native American">Native American</option>
      <option value="Indigenous Australian">Indigenous Australian</option>        
      <option value="Pacific Islander">Pacific Islander</option>
      <option value="Pacific">Pacific (Polynesian, Micronesian, etc)</option>
      <option value="Arctic">Arctic (Siberian, Eskimo)</option>
      <option value="Arab">Arab</option>
      <option value="Other">Any other ethnic group</option>
    </optgroup>
  </select>                                    
                                                                         
                                </div>
                           </div>                                     
                                                         
                           
                            
                        </div>
                      </div>
                    </div>
 
                  </div> 
              <!-- End Example Default Accordion -->
                        
                    </div> 
                      
                           <div class="form-group"> 
                    <div class="input-group"> 
                        <div class="checkbox-custom checkbox-primary" required="">
                          <input type="checkbox" id="ctlconsent" name="ctlconsent[]" value="1">
                          <label for="ctlconsent">I  have read, understand and agree to 
                              <a href="#"> Al the Terms And Conditions Listed Here </a></label> 
                        </div> 
                    </div>  
                       </div>                      
                      
                      <div class="form-group">
                          
            <div id="whats-new-submit">
				<input type="submit" class="btn btn-primary" name="aw-whats-new-submit" id="aw-whats-new-submit" value="<?php esc_attr_e( 'Submit', 'buddypress' ); ?>" />
			</div>
                <div id="frmerror"></div> 
                <div id="formsbt">
                    <button type="submit" class="btn btn-primary">Submit</button>          
                </div>
                
                        
                      </div> 
                  </div>
                    

                   


	<?php /*
	do_action( 'bp_before_activity_post_form' ); ?>

	<div id="whats-new-avatar">
		<a href="<?php echo bp_loggedin_user_domain(); ?>">
			<?php bp_loggedin_user_avatar( 'width=' . bp_core_avatar_thumb_width() . '&height=' . bp_core_avatar_thumb_height() ); ?>
		</a>
	</div>

	<p class="activity-greeting"><?php if ( bp_is_group() )
		printf( __( "What's new in %s, %s?", 'buddypress' ), bp_get_group_name(), bp_get_user_firstname( bp_get_loggedin_user_fullname() ) );
	else
		printf( __( "What's new, %s?", 'buddypress' ), bp_get_user_firstname( bp_get_loggedin_user_fullname() ) );
	?></p>
 */ ?>
    

</form><!-- #whats-new-form -->
                    
                    
                </div>
                <!-- End Example Basic Form (Form grid) -->
              </div>
            </div>
        </div>
        </div>
</div>
</div>    
 
    <script src="<?php echo get_bloginfo('template_directory'); ?>/global/js/google_auto_address.js" type="text/javascript"></script>      
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWSUfBhRbJr0WE5wNxtjTmYJSPzPtDxDE&libraries=places&callback=initAutocomplete" async defer></script>
   <?php /*
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWSUfBhRbJr0WE5wNxtjTmYJSPzPtDxDE&libraries=places&callback=initMap" async defer></script>           */ ?>
                     