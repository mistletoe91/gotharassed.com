<?php
/**
 * BuddyPress - Home
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>
<style>
.singleactivity ul#activity-stream li.timeline-reverse {
    margin: 0 auto;
    float: none;
}
.avatar {
    width: auto;
}
</style>
<div class="container"> 
    <div class="row">
        <div id="buddypress">
        
          <!-- page START -->
        <div class="page bg-white singleactivity"> 
          <!-- Forum Content -->
          <div class="page-main">

            <div id="template-notices" role="alert" aria-atomic="true">
                <?php

                /** This action is documented in bp-templates/bp-legacy/buddypress/activity/index.php */
                do_action( 'template_notices' ); ?>

            </div> 

            <div class="activity no-ajax">
                <?php if ( bp_has_activities( 'display_comments=threaded&show_hidden=true&include=' . bp_current_action() ) ) : ?>

                    <ul id="activity-stream" class="activity-list item-list">
                    <?php while ( bp_activities() ) : bp_the_activity(); ?>

                        <?php bp_get_template_part( 'activity/entry' ); ?>

                    <?php endwhile; ?>
                    </ul>

                <?php endif; ?>
            </div>
            
           </div>
           <!-- forum end -->
          </div>
          <!-- page start -->

        </div>
    </div>
</div>
