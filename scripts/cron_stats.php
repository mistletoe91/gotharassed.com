<?php

ini_set("display_errors", "0");
error_reporting(0);

require_once('../wp-config.php');
$options_ = json_decode(CUSTOM_OPTIONS, true );
 
$q_ = array (

    'ctlgender' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctlgender_you' group by meta_value "
    ),
    'ctlagegroup' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctlagegroup_you' group by meta_value "
    ),    
    'ctlfactors_physical_1' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctlfactors_physical_1' group by meta_value "
    ),    
    'ctlsev' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctlsev' group by meta_value "
    ),      
    'ctltypeofoffender' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctltypeofoffender' group by meta_value "
    ),     
    'ctlnumberofoffenders' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctlnumberofoffenders' group by meta_value "
    ),    
    'ctlfactors_more_than_one_victum' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctlfactors_more_than_one_victum' group by meta_value "
    )     
    
); 

$qExtended_ = array (     
    'total' => array (
        'query' => "SELECT count(distinct id) as mycount  FROM `xo_bp_activity` where type like 'activity_update' "
    ),    
    'ctltype' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctltype_%' group by meta_value "
    ),  
    'ctlsev_raw' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'ctlsev' group by meta_value "
    ),      
    'country_sev_1' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'country' and activity_id IN ( select activity_id from xo_bp_activity_meta where meta_key like 'ctlsev' and meta_value like '1' ) group by meta_value"
    ),    
    'country_sev_2' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'country' and activity_id IN ( select activity_id from xo_bp_activity_meta where meta_key like 'ctlsev' and meta_value like '2' ) group by meta_value"
    ),
    'country_sev_3' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'country' and activity_id IN ( select activity_id from xo_bp_activity_meta where meta_key like 'ctlsev' and meta_value like '3' ) group by meta_value"
    ),
    'country_sev_4' => array (
        'query' => "SELECT count( activity_id) as mycount, meta_value as myval FROM `xo_bp_activity_meta` where meta_key like 'country' and activity_id IN ( select activity_id from xo_bp_activity_meta where meta_key like 'ctlsev' and meta_value like '4' ) group by meta_value"
    ),      
 
);



// Create connection
$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$final_ = array ();
//run regular queries
foreach ($q_ as $i=>$v ){ 
        $result = $conn->query( $v['query']); 
        $recordCount = 0;
        $rows = array();
        if ($result->num_rows > 0) { 
            while($row = $result->fetch_assoc()) {  
                $rows [ $options_ [$i][$row ['myval']]['Name']  ] =  $row ['mycount']; 
            }#end while
        }     
        $final_ [$i] = $rows ;
}#end foreach 


#Run query one by one from extended queries coz they need special attention
$result = $conn->query( $qExtended_['total']['query']); 
$recordCount = 0;
$rows = array();
if ($result->num_rows > 0) { 
        while($row = $result->fetch_assoc()) {  
            $rows [ 0 ] =  $row ['mycount']; 
            break;
        }#end while
        $final_ ['total'] = $rows ;
}#endif  

$i = 'ctlsev_raw';
$result = $conn->query( $qExtended_[$i ]['query']); 
$recordCount = 0;
$rows = array();
if ($result->num_rows > 0) { 
        while($row = $result->fetch_assoc()) {  
            $rows [ $row ['myval']]   =  $row ['mycount']  ; 
        }#end while
        $final_ [$i ] = $rows ;
        $final_ ['loc_sev'] = $rows;
}#endif   

$listOfCountries = array ();

//Repeated code. we can put it in loop
$i = 'country_sev_1';
$result = $conn->query( $qExtended_[$i ]['query']); 
$recordCount = 0;
$rows = array();
if ($result->num_rows > 0) { 
        while($row = $result->fetch_assoc()) {  
            $rows [ $row ['myval']]   =   array ( 1 => $row ['mycount'] ); 
        }#end while
        $final_ [$i ] = $rows ;
        $final_ ['loc_sev'] = $rows;
        foreach(array_keys($rows ) as $indx_=>$cntName){
           $listOfCountries [$cntName] = true;
        }//end foreach
}#endif   
$i = 'country_sev_2';
$result = $conn->query( $qExtended_[$i ]['query']); 
$recordCount = 0;
$rows = array();
if ($result->num_rows > 0) { 
        while($row = $result->fetch_assoc()) {  
            
            if(isset( $final_['loc_sev'][  $row ['myval'] ]  )){
                $final_['loc_sev'][ $row ['myval']][2] = $row ['mycount'] ;
            } else {
                $final_['loc_sev'][ $row ['myval']][2] = $row ['mycount'] ; 
            }#endif
            
            $rows [ $row ['myval']]   =   array (2 => $row ['mycount'] ); 
        }#end while
        $final_ [$i ] = $rows ;
        foreach(array_keys($rows ) as $indx_=>$cntName){
           $listOfCountries [$cntName] = true;
        }//end foreach
}#endif   
$i = 'country_sev_3';
$result = $conn->query( $qExtended_[$i ]['query']); 
$recordCount = 0;
$rows = array();
if ($result->num_rows > 0) { 
        while($row = $result->fetch_assoc()) {  
            
            if(isset( $final_['loc_sev'][  $row ['myval'] ]  )){
                $final_['loc_sev'][ $row ['myval']][3] = $row ['mycount'] ;
            } else {
                $final_['loc_sev'][ $row ['myval']][3] = $row ['mycount'] ;                 
            }#endif
            
            $rows [ $row ['myval']]   =   array ( 3 => $row ['mycount'] ); 
        }#end while
        $final_ [$i ] = $rows ;
        foreach(array_keys($rows ) as $indx_=>$cntName){
           $listOfCountries [$cntName] = true;
        }//end foreach
}#endif   
$i = 'country_sev_4';
$result = $conn->query( $qExtended_[$i ]['query']); 
$recordCount = 0;
$rows = array();
if ($result->num_rows > 0) { 
        while($row = $result->fetch_assoc()) {  
            
            if(isset( $final_['loc_sev'][  $row ['myval'] ]  )){
                $final_['loc_sev'][ $row ['myval']][4] = $row ['mycount'] ;
            } else {
                $final_['loc_sev'][ $row ['myval']][4] = $row ['mycount'] ;                 
            }#endif
            
            $rows [ $row ['myval']]   =   array ( 4 => $row ['mycount'] ); 
        }#end while
        $final_ [$i ] = $rows ;
        foreach(array_keys($rows ) as $indx_=>$cntName){
           $listOfCountries [$cntName] = true;
        }//end foreach
}#endif




$refinedCtlTypes = array ();
foreach ($options_ ['ctltype']['optgroup'] as $i=>$v){
    foreach ($v ['options'] as $ii=>$vv){
         if($vv ['Name']){
            $refinedCtlTypes [$ii] = $vv ['Name']; 
         }
    }#end foreach  
}#end foreach

$result = $conn->query( $qExtended_['ctltype']['query']); 
$recordCount = 0;
$rows = array();
if ($result->num_rows > 0) { 
        while($row = $result->fetch_assoc()) {   
            $key_ = $refinedCtlTypes [$row ['myval']];
            if($key_){
                $rows [  $key_  ] =  $row ['mycount'];    
            }else {
                //The following line is wrong so commenting it. The unspecified wont even get counted
                #$rows [  'Unspecified'  ] =  $row ['mycount'];    
            }            
        }#end while
        $final_ ['ctltype'] = $rows ;
}#endif    

unset ($final_ ['country_sev_1']);
unset ($final_ ['country_sev_2']);
unset ($final_ ['country_sev_3']);
unset ($final_ ['country_sev_4']);

 //Anybody need further calculations ? 
$final_ ['ctlfactors_physical_1']['No'] = ($final_ ['total'][0] -  $final_ ['ctlfactors_physical_1']['Yes']);

//Now make it all percentage except TOTAL
if(true){
    
    //loc_sev - this should happen before 
    $sevCount = array ();
    foreach ($final_ ['loc_sev'] as $country_=>$arr_){
      
        if(!isset( $listOfCountries[$country_] )){
               unset( $final_ ['loc_sev'][$country_] );
        }//endif

        foreach ($arr_ as $sev_=>$count_){ 
            $final_ ['loc_sev'][$country_][$sev_] = decimal2places(($final_ ['loc_sev'][$country_][$sev_]/ $final_ ['ctlsev_raw'][$sev_] )*100);
        }//end foreach 
    }#end foreach
     
    foreach ($final_ as $key_=>$value){
        if($key_ === 'total' || $key_ === 'loc_sev'){
            continue;
        }#endif

        $total_ = 0;
        foreach ($final_[$key_] as $i=>$v){
            $total_ += $v;
        }#end foreach
        foreach ($final_[$key_] as $i=>$v){
            $final_[$key_][$i] = decimal2places(($v/$total_)*100);
        }#end foreach    
    }#end foreach

    
    
}#endif




 
  
        $jTableResult = array();
        $jTableResult['time'] =   date('m/d/Y H:i:s', time()); 
        $jTableResult['data'] = $final_;
        print json_encode($jTableResult, JSON_PRETTY_PRINT);
    
//Functions 
function decimal2places ($number){
    return number_format((float)$number, 2, '.', '');
}