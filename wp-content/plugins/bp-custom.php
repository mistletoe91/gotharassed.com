<?php
 

function bpfr_addmore() {

	// bail if activity component is not used
	if ( !bp_is_active( 'activity' ) ) {
		return false;
	} 
    
	// action
	$optionsArray = json_decode(CUSTOM_OPTIONS, true);
	$returnI = bp_activity_get_meta( bp_get_activity_id(), 'ctltype' );
    if($returnI){
        $returnI = json_decode ($returnI, true); 
        if(isset($returnI ['raw'])){
            
            $returnHTML  = '';
             
            foreach ($returnI ['raw'] as $i=>$v){
                 
                if(isset( $optionsArray ['ctltype']['optgroup'][0]['options'][$v] )){                      
                  $returnHTML .= '<span><i class="fa fa-tag mytagoptions" aria-hidden="true"></i> '.   $optionsArray ['ctltype']['optgroup'][0]['options'][$v]['Name'].'</span>';         
                }//endif 
                
            }//end foreach 
        
        echo "<div class='categories'> $returnHTML </div>";            
            
        }//endif
    }//endif 

        #$ctlsev_ = explode ("SEV", $ctlsev);
        #preg_match('/^SEV(\d)/i', $ctlsev, $ctlsev_);
        #$selectedSev = strtoupper($ctlsev_ [0]);
          



}#end function
add_action( 'bp_activity_entry_meta', 'bpfr_addmore' );

/*
function bpfr_fav_count() {

	// bail if activity component is not used
	if ( !bp_is_active( 'activity' ) ) {
		return false;
	} 
    
	// action
	$optionsArray = json_decode(CUSTOM_OPTIONS, true);
	$returnI = bp_activity_get_meta( bp_get_activity_id(), 'ctltype' );
        $ctlsev = $optionsArray['ctltype'][$returnI]['Name'];

        #$ctlsev_ = explode ("SEV", $ctlsev);
        #preg_match('/^SEV(\d)/i', $ctlsev, $ctlsev_);
        #$selectedSev = strtoupper($ctlsev_ [0]);
        return;
        echo "
<div class='custommeta'>
       $ctlsev 
</div>
        ";


}#end function
add_action( 'bp_activity_entry_meta', 'bpfr_fav_count' );
*/


function add_meta_to_activity( $content, $user_id, $activity_id ) {
  
    $safePost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
  
    
    $fieldsList = array ( 
       'street_number'
        ,'route'
        ,'locality'
        ,'administrative_area_level_1'
        ,'postal_code'
        ,'country'
        ,'ctlsev' 
        ,'ctldate'
        ,'ctltime'
        ,'ctlnumberofoffenders'
        ,'ctltypeofoffender' 
        ,'ctlagegroup'
        ,'ctlrace'
        ,'ctlprimarylang' 
        ,'ctlgender'         
        ,'ctlgender_you' 
        ,'ctlagegroup_you'
        ,'ctlprimarylang_you'
        ,'ctlrace_you' 
    );
    
    foreach ($fieldsList as $i=>$v){
        if(isset($safePost [$v]) && $safePost [$v] ){
            bp_activity_update_meta( $activity_id, $v , $safePost [$v] );
        }#end if
    }#end foreach 
    
    //Array Field
    $fieldsList = array ( 
       'ctltype',
        'ctlsubtype',
        'ctlfactors_physical',
        'ctlfactors_more_than_one_victum'
    );
    
    //Store multi select as raw json in addition to per line
    $multiSelect = array ();
    $multiSelect ['ctltype'] = array ('raw'=>array());
    $multiSelect ['ctlsubtype'] = array ('raw'=>array()); 
    
    foreach ($fieldsList as $i=>$v){
        if(isset($safePost [$v]) && $safePost [$v] ){
            foreach ($safePost [$v] as $ii=>$vv){ 
                
                //also store in raw form 
                if(isset($multiSelect [$v] )){ 
                    if(isset($multiSelect [$v] ['raw'] )){
                        array_push ($multiSelect [$v] ['raw'] , $vv ); 
                    }//endif
                }//endif
                
                bp_activity_update_meta( $activity_id, $v.'_'.$vv , $vv );
            }
        }#end if
    }#end foreach   
     
    
    //Store multi select as raw json in addition to per line
    foreach ($multiSelect as $i=>$v){
        bp_activity_update_meta( $activity_id, $i  , json_encode($v   ) );
    }#end foreach 

}
add_action( 'bp_activity_posted_update', 'add_meta_to_activity', 10, 3 );
 