<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/

function endsWith($haystack, $needle)
{
    $length = strlen($needle);

    return $length === 0 || 
    (substr($haystack, -$length) === $needle);
}
$anyError_ = '';
if (!empty($_SERVER['HTTP_REFERER'])) { 
    if(endsWith($_SERVER['HTTP_REFERER'] ,'gotharassed.com/reports/')){
        $anyError_ = '<div class="alert alert-warning" role="alert">Please review the form to register</div>';
    }//endif
}

?>
<div class="tml tml-register" id="theme-my-login<?php $template->the_instance(); ?>">
	<?php #$template->the_action_template_message( 'register' );     
    ?>  
	<?php $template->the_errors(); ?> 
    <?php echo $anyError_;?>
    <div id="gt_tiles">  
                    <div class="tile-progress tile-blue">
                        <div class="tile-header">
                            <h3>Register </h3>
                            <div class="loginavatar"><i class="fa fa-user-secret" aria-hidden="true"></i></div> 
                        </div>
                        <div class="tile-progressbar">
                            <span data-fill="50%" style="width: 50%;"></span>
                        </div>
                        <div class="tile-footer"> 
                            <span>Please proceed with quick registration !</span>
                        </div>
                        <div class="tile-form">  
                             <div class="divtml_form">
                                 
    <?php 
                                 #$template->the_action_url( 'register', 'login_post' ); 
    ?>                                 
	<form name="registerform" id="registerform<?php $template->the_instance(); ?>" action="/register/" method="post">
		<?php if ( 'email' != $theme_my_login->get_option( 'login_type' ) ) : ?>
		<p class="tml-user-login-wrap">
			<label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Username', 'theme-my-login' ); ?></label>
			<input type="text" name="user_login" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_login' ); ?>" size="20" />
		</p>
		<?php endif; ?>

		<p class="tml-user-email-wrap">
			<label for="user_email<?php $template->the_instance(); ?>"><?php _e( 'E-mail', 'theme-my-login' ); ?></label>
			<input type="text" name="user_email" id="user_email<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_email' ); ?>" size="20" />
		</p>
        
        <?php do_action( 'register_form' ); ?>
		<p class="tml-submit-wrap">
			<input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Register', 'theme-my-login' ); ?>" />
			<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'register' ); ?>" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="action" value="register" />
		</p>   
        
        
        <?php $template->the_action_links( array( 'register' => false ) ); ?>
         

		
        <?php /*
		<p class="tml-registration-confirmation" id="reg_passmail<?php $template->the_instance(); ?>"><?php echo apply_filters( 'tml_register_passmail_template_message', __( 'Registration confirmation will be e-mailed to you.', 'theme-my-login' ) ); ?></p>
        */ ?>


	</form>
	
                            </div>
                                
                        </div>                        
                    </div> 
    </div>
     

</div>
