<?php  
get_header();  
?>  
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/form/flipcard.css">  

<section class="container-wrap main-color">
	<div id="main-container" class="container">
		<div class="row"> 
			<div class="template-page col-sm-12 tpl-no">
				<div class="wrap-content"> 
<!-- Begin Article -->
<article class="clearfix  page type-page status-publish"> 
	<div class="article-content contactpage" id="whats-new-form">
   
        
    
 
        <div class="container-fluid" id="whats-new-form">
                  <div class="row"> 
                       <div class="col-xs-12 col-sm-12 col-md-6">     
                           
                           <?php echo do_shortcode(" [ninja_form id=1]"); ?>

                          </div>                              
                      <div class="col-xs-12 col-sm-12 col-md-6"> 
                           <div class="[ info-card ]"> 
                                <div class="bigicon symbolstats"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                <div class="[ info-card-details ] animate">
                                    <div class="[ info-card-header ]">
                                        <h1> Contact us  </h1>
                                        <h3>Fill the form to reach out to us</h3>
                                    </div>
                                    <div class="[ info-card-detail ]">
                                        <!-- Description -->
                                        <p>Have a question, comment or suggestions ? Feel free to reach out to us by submitting this form. We love to hear from you</p>
                                        <div class="social"> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                  </div>
        </div>         
        
        
	</div><!--end article-content--> 
</article>
<!-- End  Article --> 
				</div><!--end wrap-content-->
			</div><!--end main-page-template-->
						</div><!--end .row-->		
    </div><!--end .container--> 
</section> 
   
<?php  
get_footer();  
?> 