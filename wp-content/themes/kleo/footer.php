<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Kleo
 * @since Kleo 1.0
 */
?>

			<?php
			/**
			 * After main part - action
			 */
			do_action('kleo_after_main');
			?>

		</div><!-- #main -->

		<?php get_sidebar('footer');?>
	
		<?php 
		/**
		 * After footer hook
		 * @hooked kleo_go_up
		 * @hooked kleo_show_contact_form
		 */
		do_action('kleo_after_footer');
		?>

	</div><!-- #page -->

    <?php
    /**
     * After page hook
     * @hooked kleo_show_side_menu 10
     */
    do_action('kleo_after_page');
    ?>

	<!-- Analytics -->
	<?php echo sq_option('analytics', ''); ?>

	<?php wp_footer(); ?>  
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/custom.js'></script>
 
<?php
if(false){
    
    $cookie_name = "ghapp";
    $cookie_value = "1";

    if(isset($_GET['source_app']) && $_GET['source_app']){
        if(!isset($_COOKIE[$cookie_name])){
            setcookie($cookie_name, $cookie_value, time() + (6* 86400 * 30), "/");//6 months
        }//endif        
    }//endif
     
    if(isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name]) {
        echo "
        <link rel='stylesheet' href='".get_template_directory_uri()."/assets/css/gtapp.css'>
        <script type='text/javascript' src='".get_template_directory_uri()."/assets/js/gtapp.js'></script>
        ";
    }//endif    
    
} 
?>
</body>
</html>