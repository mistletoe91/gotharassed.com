<?php
$optionsArray = json_decode(CUSTOM_OPTIONS, true);
function displayOptions ( $optionsArray, $optionName)                      
{
    $return = '';
    foreach($optionsArray [$optionName] as $i=>$v ){
        
        if($i === 'optgroup'){
             foreach($v  as $ii=>$vv ){
              $return .= '<optgroup label="'.$vv ['label'].'">';
                foreach($vv ['options'] as $iii=>$vvv ){
                 $return .= '<option value="'.$iii.'">'.$vvv ['Name'].'</option>';
                }//end foreach
             }//end foreach
            $return .= "</optgroup>";
        } else {
            $selected_ = '';
            if(isset($v ['Selected']) && $v ['Selected']){
                $selected_ = 'selected';
            }//endif
            $return .= '<option value="'.$i.'" '.$selected_.'>'.$v ['Name'].'</option>';
        }//endif        

    }//end foreach
    return $return;
}
?> 


      
<form action="<?php bp_activity_post_form_action(); ?>" class="ui form" method="post" id="whats-new-form" name="whats-new-form"> 
<div class="row" style="margin-top:20px; " id="rowmainform">
  <div class="col-md-12">   
    <div class="panel-group accordionclass"  id="accordion1" role="tablist" aria-multiselectable="true">    
        <div class="panel panel-default panel-mainform" id="divaccordion_1">
            <div class="panel-heading" role="tab" id="heading1_1">
              <h4 class="panel-title">
                <a id="collapse1_1_btn" class="collapse_a_heading" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_1" aria-expanded="true" aria-controls="collapse1_1">File Harassment Claim</a>
              </h4>
            </div>
            <div id="collapse1_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">    

                <div class="row">  

                    <div class="col-md-6"> 


                          <div class="mainselect_div"> 
                            <label class="form-control-label" for="ctlsev">Severity</label>                         
                             <select class="" id="ctlsev"  name="ctlsev">  
                                <?php echo displayOptions ( $optionsArray, 'ctlsev'); ?> 
                            </select>                        
                          </div>



                          <div class="mainselect_div">     
                              <label class="form-control-label"  >Involve Physical Harassment</label>   
                                <div class="funkyradio">                              
                            <div class="funkyradio-primary">
                                <input type="checkbox" id="ctlfactors_physical" name="ctlfactors_physical[]" > 
                                <label  id="lblctlfactors_physical" for="ctlfactors_physical">No</label></div>                                
                                </div>                
                        </div>

                          <div class="mainselect_div">     
                              <label class="form-control-label"  >More Than One Victim</label>   
                                <div class="funkyradio">                              
                            <div class="funkyradio-primary">
                                <input   type="checkbox" id="ctlfactors_more_than_one_victum" name="ctlfactors_more_than_one_victum[]" > 
                                <label id="lblctlfactors_morethanone"  for="ctlfactors_more_than_one_victum">No</label></div>                                
                                </div>                
                        </div>                

                          <div class="mainselect_div"> 
                             <label class="form-control-label" for="ctltype">Type of Harrasement</label>   
                             <select class="" id="ctltype"  name="ctltype[]" multiple>  
                                <?php echo displayOptions ( $optionsArray, 'ctltype'); ?> 
                            </select>                        
                          </div>                





                    </div>

                    <div class="col-md-6">   

                        <div class="[ info-card ]">
                            <img   src="/img/cake.svg" alt="Cake" />
                            <div class="[ info-card-details ] animate">
                                <div class="[ info-card-header ]">
                                    <h1> Order Your Cake  </h1>
                                    <h3> Free Delivery in GTA within 24-48 hrs  </h3>
                                </div>
                                <div class="[ info-card-detail ]">
                                    <!-- Description -->
                                    <p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
                                    <div class="social"> 
                                    </div>
                                </div>
                            </div>
                        </div>                



                    </div>             
                </div>       

                <div class="row">
                    <div class="col-md-12 btnrow">    
                        <a id="href_1"  class="href_1 btn  btn-allreports mainbtns btn-primary"><i class="fa fa-chevron-right" aria-hidden="true"></i> Next - Incident Detail</a>
                    </div>
                </div>



              </div>
            </div>
          </div>  
        <div class="panel panel-default panel-mainform" id="divaccordion_2">
            <div class="panel-heading" role="tab" id="heading1_2">
              <h4 class="panel-title">
                <a id="collapse1_2_btn" class="collapsed collapse_a_heading" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_2" aria-expanded="false" aria-controls="collapse1_2">
                  Incident Detail
                </a>
              </h4>
            </div>
            <div id="collapse1_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">

                <div class="row">  
                    <div class="col-md-12">  


                              <div class="form-group">
                                <label class="form-control-label" for="whats-new">Description Of Incident</label>  
                <div id="whats-new-textarea">
        <textarea cols="200" class="bp-suggestions form-control" name="whats-new" id="whats-new" cols="50" rows="10"
                        <?php if ( bp_is_group() ) : ?>data-suggestions-group-id="<?php echo esc_attr( (int) bp_get_current_group_id() ); ?>" <?php endif; ?>
                    ><?php if ( isset( $_GET['r'] ) ) : ?>@<?php echo esc_textarea( $_GET['r'] ); ?> <?php endif; ?></textarea>  
                    <span class="text-help">Please provide as much detail as possible </span>

                </div>


            <div id="whats-new-content">


                <div id="whats-new-options">


                    <?php if ( bp_is_active( 'groups' ) && !bp_is_my_profile() && !bp_is_group() ) : ?>

                        <div id="whats-new-post-in-box">

                            <?php _e( 'Post in', 'buddypress' ); ?>:

                            <label for="whats-new-post-in" class="bp-screen-reader-text"><?php
                                /* translators: accessibility text */
                                _e( 'Post in', 'buddypress' );
                            ?></label>
                            <select id="whats-new-post-in" name="whats-new-post-in">
                                <option selected="selected" value="0"><?php _e( 'My Profile', 'buddypress' ); ?></option>

                                <?php if ( bp_has_groups( 'user_id=' . bp_loggedin_user_id() . '&type=alphabetical&max=100&per_page=100&populate_extras=0&update_meta_cache=0' ) ) :
                                    while ( bp_groups() ) : bp_the_group(); ?>

                                        <option value="<?php bp_group_id(); ?>"><?php bp_group_name(); ?></option>

                                    <?php endwhile;
                                endif; ?>

                            </select>
                        </div>
                        <input type="hidden" id="whats-new-post-object" name="whats-new-post-object" value="groups" />

                    <?php elseif ( bp_is_group_activity() ) : ?>

                        <input type="hidden" id="whats-new-post-object" name="whats-new-post-object" value="groups" />
                        <input type="hidden" id="whats-new-post-in" name="whats-new-post-in" value="<?php bp_group_id(); ?>" />

                    <?php endif; ?>

                    <?php

                    /**
                     * Fires at the end of the activity post form markup.
                     *
                     * @since 1.2.0
                     */
                    do_action( 'bp_activity_post_form_options' ); ?>

                </div><!-- #whats-new-options -->
            </div><!-- #whats-new-content -->



            <?php wp_nonce_field( 'post_update', '_wpnonce_post_update' ); ?>
            <?php 
            do_action( 'bp_after_activity_post_form' ); ?>                          

                              </div>   

                    </div> 
                </div>   
                <!-- End Row -->

                <div class="row">
                    <div class="col-md-12 btnrow">    
                        <a id="href_2"  class="href_2 btn  btn-allreports mainbtns btn-primary"><i class="fa fa-chevron-right" aria-hidden="true"></i> Next - Location & Date</a>
                    </div>
                </div>

              </div>
            </div>
          </div>   
        <div class="panel panel-default panel-mainform" id="divaccordion_3">
            <div class="panel-heading" role="tab" id="heading1_3">
              <h4 class="panel-title">
                <a id="collapse1_3_btn" class="collapsed collapse_a_heading" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_3" aria-expanded="false" aria-controls="collapse1_3">
                  Location & Date
                </a>
              </h4>
            </div>
            <div id="collapse1_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                <div class="row"> 
                    <div class="col-md-6"> 
                          <div style="margin-top:10px;"></div>
                          <div class="input-group">      
                            <input  type="text" onFocus="geolocate()"  id="txtincident_location" name="txtincident_location"   >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label  class="fancytextinputlabels">Incident Location</label>
                          </div>

                          <div class="input-group">      
                            <input   name="ctldate" id="ctldate"  type="text"   >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels">Date of Incident</label>
                          </div> 



                          <div class="input-group">      
                            <input name="ctltime" id="ctltime"  type="text"   >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                              <label class="fancytextinputlabels">Time of Incident</label>
                          </div>              
                    </div>   


                    <div class="col-md-6">   

                        <div class="[ info-card ]">
                            <img   src="/img/map.png" alt="Security Seal" />
                            <div class="[ info-card-details ] animate">
                                <div class="[ info-card-header ]">
                                    <h1> Always FREE Delivery  </h1>
                                    <h3> We make sure to deliver fast </h3>
                                </div>
                                <div class="[ info-card-detail ]">
                                    <!-- Description -->
                                    <p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
                                    <div class="social"> 
                                    </div>
                                </div>
                            </div>
                        </div>                

                             <!-- GEO LOCATION START -->

        <table id="address" class="hide">
              <tr>
                <td class="label">Street address</td>
                <td class="slimField"><input class="field" id="street_number" name="street_number"
                      disabled="true"></input></td>
                <td class="wideField" colspan="2"><input class="field" id="route" name="route"
                      disabled="true"></input></td>
              </tr>
              <tr>
                <td class="label">City</td>
                <!-- Note: Selection of address components in this example is typical.
                     You may need to adjust it for the locations relevant to your app. See
                     https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
                -->
                <td class="wideField" colspan="3"><input class="field" id="locality" name="locality"
                      disabled="true"></input></td>
              </tr>
              <tr>
                <td class="label">State</td>
                <td class="slimField"><input class="field"
                      id="administrative_area_level_1" name="administrative_area_level_1" disabled="true"></input></td>
                <td class="label">Zip code</td>
                <td class="wideField"><input class="field" id="postal_code" name="postal_code"
                      disabled="true"></input></td>
              </tr>
              <tr>
                <td class="label">Country</td>
                <td class="wideField" colspan="3"><input class="field" name="country"
                      id="country" disabled="true"></input></td>
              </tr>
            </table>



                    </div>

                   <div class="col-md-6">  


                    </div>
                 </div>
                <!-- End Row -->

                <div class="row">
                    <div class="col-md-12 btnrow">    
                        <a id="href_3"  class="href_3 btn  btn-allreports mainbtns btn-primary"><i class="fa fa-chevron-right" aria-hidden="true"></i> Next -  Offender`s Profile</a>
                    </div>
                </div>          

              </div>
            </div>
          </div> 
        <div class="panel panel-default panel-mainform" id="divaccordion_4">
            <div class="panel-heading" role="tab" id="heading1_4">
              <h4 class="panel-title">
                <a id="collapse1_4_btn" class="collapsed collapse_a_heading" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_4" aria-expanded="false" aria-controls="collapse1_4">
                  Offender`s Profile
                </a>
              </h4>
            </div>
            <div id="collapse1_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">

                    <p>Please use below form to fill down offender`s profile. In case of multiple offender, please fill the primary offender`s profile and other related information</p>

        <!-- Offender`s Identity Start -->

                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlnumberofoffenders">Number of Offenders</label>
                                        <div class=""> 
                                         <select class="" id="ctlnumberofoffenders" name="ctlnumberofoffenders"  data-plugin="select2">  
                                            <?php echo displayOptions ( $optionsArray, 'ctlnumberofoffenders'); ?>   
                                        </select> 
                                        </div>
                                   </div>  

                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctltypeofoffender">Type of Offender</label>   
                                        <div class=""> 
                                         <select class="" id="ctltypeofoffender" name="ctltypeofoffender"  data-plugin="select2">
                                            <?php echo displayOptions ( $optionsArray, 'ctltypeofoffender'); ?>
                                        </select> 
                                        </div>
                                   </div>                      

                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlgender">Offender`s Gender</label>   
                                        <div class=""> 
                                         <select class="" id="ctlgender" name="ctlgender"  data-plugin="select2">  
                                             <?php echo displayOptions ( $optionsArray, 'ctlgender'); ?>

                                        </select> 
                                        </div>
                                   </div> 

                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlagegroup">Offender`s Age Group</label>
                                        <div class=""> 
                                         <select class="" id="ctlagegroup" name="ctlagegroup"  data-plugin="select2">  
                                             <?php echo displayOptions ( $optionsArray, 'ctlagegroup'); ?>

                                        </select> 
                                        </div>
                                   </div> 


                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlrace">Offender`s Ethnicity </label>   
                                        <div class="">  

        <select class=" dropdown" id="ctlrace" name="ctlrace"  data-plugin="select2">
            <option value="" selected="selected" disabled="disabled">-- select one --</option>
            <optgroup label="White">
              <option value="White English">English</option>
              <option value="White Welsh">Welsh</option>
              <option value="White Scottish">Scottish</option>
              <option value="White Northern Irish">Northern Irish</option>
              <option value="White Irish">Irish</option>
              <option value="White Gypsy or Irish Traveller">Gypsy or Irish Traveller</option>
              <option value="White Other">Any other White background</option>
            </optgroup>
            <optgroup label="Mixed or Multiple ethnic groups">
              <option value="Mixed White and Black Caribbean">White and Black Caribbean</option>
              <option value="Mixed White and Black African">White and Black African</option>
              <option value="Mixed White Other">Any other Mixed or Multiple background</option>
            </optgroup>
            <optgroup label="Asian">
              <option value="Asian Indian">Indian</option>
              <option value="Asian Pakistani">Pakistani</option>
              <option value="Asian Bangladeshi">Bangladeshi</option>
              <option value="Asian Chinese">Chinese</option>
              <option value="Thai, Malay, Filipino">Thai, Malay, Filipino</option>   
              <option value="North East Asian">North East Asian (Mongol, Tibetan, Korean Japanese, etc)</option>           
              <option value="Asian Other">Any other Asian background</option>
            </optgroup>
            <optgroup label="Black">
              <option value="Black African">African</option>
              <option value="Black African American">African American</option>
              <option value="Black Caribbean">Caribbean</option>
              <option value="Black Other">Any other Black background</option>
            </optgroup>
            <optgroup label="Other ethnic groups">      
              <option value="Hispanic">Hispanic</option>
              <option value="Latino">Latino</option>
              <option value="Native American">Native American</option>
              <option value="Indigenous Australian">Indigenous Australian</option>        
              <option value="Pacific Islander">Pacific Islander</option>
              <option value="Pacific">Pacific (Polynesian, Micronesian, etc)</option>
              <option value="Arctic">Arctic (Siberian, Eskimo)</option>
              <option value="Arab">Arab</option>
              <option value="Other">Any other ethnic group</option>
            </optgroup>
          </select>                                    

                                        </div>
                                   </div>   


                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlprimarylang">Offender`s Primary Language (If Known)</label>   
                                        <div class=""> 
                                         <select class="" id="ctlprimarylang" name="ctlprimarylang"  data-plugin="select2"> 
          <option value="AF">Afrikanns</option>
          <option value="SQ">Albanian</option>
          <option value="AR">Arabic</option>
          <option value="HY">Armenian</option>
          <option value="EU">Basque</option>
          <option value="BN">Bengali</option>
          <option value="BG">Bulgarian</option>
          <option value="CA">Catalan</option>
          <option value="KM">Cambodian</option>
          <option value="ZH">Chinese (Mandarin)</option>
          <option value="HR">Croation</option>
          <option value="CS">Czech</option>
          <option value="DA">Danish</option>
          <option value="NL">Dutch</option>
          <option value="EN">English</option>
          <option value="ET">Estonian</option>
          <option value="FJ">Fiji</option>
          <option value="FI">Finnish</option>
          <option value="FR">French</option>
          <option value="KA">Georgian</option>
          <option value="DE">German</option>
          <option value="EL">Greek</option>
          <option value="GU">Gujarati</option>
          <option value="HE">Hebrew</option>
          <option value="HI">Hindi</option>
          <option value="HU">Hungarian</option>
          <option value="IS">Icelandic</option>
          <option value="ID">Indonesian</option>
          <option value="GA">Irish</option>
          <option value="IT">Italian</option>
          <option value="JA">Japanese</option>
          <option value="JW">Javanese</option>
          <option value="KO">Korean</option>
          <option value="LA">Latin</option>
          <option value="LV">Latvian</option>
          <option value="LT">Lithuanian</option>
          <option value="MK">Macedonian</option>
          <option value="MS">Malay</option>
          <option value="ML">Malayalam</option>
          <option value="MT">Maltese</option>
          <option value="MI">Maori</option>
          <option value="MR">Marathi</option>
          <option value="MN">Mongolian</option>
          <option value="NE">Nepali</option>
          <option value="NO">Norwegian</option>
          <option value="FA">Persian</option>
          <option value="PL">Polish</option>
          <option value="PT">Portuguese</option>
          <option value="PA">Punjabi</option>
          <option value="QU">Quechua</option>
          <option value="RO">Romanian</option>
          <option value="RU">Russian</option>
          <option value="SM">Samoan</option>
          <option value="SR">Serbian</option>
          <option value="SK">Slovak</option>
          <option value="SL">Slovenian</option>
          <option value="ES">Spanish</option>
          <option value="SW">Swahili</option>
          <option value="SV">Swedish </option>
          <option value="TA">Tamil</option>
          <option value="TT">Tatar</option>
          <option value="TE">Telugu</option>
          <option value="TH">Thai</option>
          <option value="BO">Tibetan</option>
          <option value="TO">Tonga</option>
          <option value="TR">Turkish</option>
          <option value="UK">Ukranian</option>
          <option value="UR">Urdu</option>
          <option value="UZ">Uzbek</option>
          <option value="VI">Vietnamese</option>
          <option value="CY">Welsh</option>
          <option value="XH">Xhosa</option>
        </select>                       
                                        </div>
                                   </div>                       
                                          <!-- Offender`s Identity Stop -->                      



                    </div>
                    <div class="col-md-6">

                        <div class="[ info-card ]">
                            <img   src="/img/map.png" alt="Security Seal" />
                            <div class="[ info-card-details ] animate">
                                <div class="[ info-card-header ]">
                                    <h1> Always FREE Delivery  </h1>
                                    <h3> We make sure to deliver fast </h3>
                                </div>
                                <div class="[ info-card-detail ]">
                                    <!-- Description -->
                                    <p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
                                    <div class="social">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 btnrow">    
                        <a id="href_4"  class="href_4 btn  btn-allreports mainbtns btn-primary"><i class="fa fa-chevron-right" aria-hidden="true"></i> Final Step -  
        Optional Voluntary Disclosure</a>
                    </div>
                </div>          

              </div>
            </div> 
        </div> 
        <div class="panel panel-default panel-mainform" id="divaccordion_5">
            <div class="panel-heading" role="tab" id="heading1_5">
              <h4 class="panel-title">
                <a id="collapse1_5_btn" class="collapsed collapse_a_heading" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_5" aria-expanded="false" aria-controls="collapse1_5">
                  Final Step - Optional Voluntary Disclosure
                </a>
              </h4>
            </div>
            <div id="collapse1_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">

                    <p>Information below is optional and does not get displayed along with your record. We highly encourage to fill this information it is used as an aggregate for statistical purposes.</p>


                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlgender_you">Your Gender</label>
                                        <div class="">
                                         <select class="" id="ctlgender_you" name="ctlgender_you"  data-plugin="select2">
                                             <?php echo displayOptions ( $optionsArray, 'ctlgender'); ?>

                                        </select> 
                                        </div>
                                   </div>

                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlagegroup_you">Your Age Group</label>   
                                        <div class=""> 
                                         <select class="" id="ctlagegroup_you" name="ctlagegroup_you"  data-plugin="select2"> 
                                              <?php echo displayOptions ( $optionsArray, 'ctlagegroup'); ?> 
                                        </select> 
                                        </div>
                                   </div> 

                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlprimarylang_you">Your Primary Language</label>   
                                        <div class=""> 
                                         <select class="" id="ctlprimarylang_you" name="ctlprimarylang_you"  data-plugin="select2"> 
          <option value="AF">Afrikanns</option>
          <option value="SQ">Albanian</option>
          <option value="AR">Arabic</option>
          <option value="HY">Armenian</option>
          <option value="EU">Basque</option>
          <option value="BN">Bengali</option>
          <option value="BG">Bulgarian</option>
          <option value="CA">Catalan</option>
          <option value="KM">Cambodian</option>
          <option value="ZH">Chinese (Mandarin)</option>
          <option value="HR">Croation</option>
          <option value="CS">Czech</option>
          <option value="DA">Danish</option>
          <option value="NL">Dutch</option>
          <option value="EN">English</option>
          <option value="ET">Estonian</option>
          <option value="FJ">Fiji</option>
          <option value="FI">Finnish</option>
          <option value="FR">French</option>
          <option value="KA">Georgian</option>
          <option value="DE">German</option>
          <option value="EL">Greek</option>
          <option value="GU">Gujarati</option>
          <option value="HE">Hebrew</option>
          <option value="HI">Hindi</option>
          <option value="HU">Hungarian</option>
          <option value="IS">Icelandic</option>
          <option value="ID">Indonesian</option>
          <option value="GA">Irish</option>
          <option value="IT">Italian</option>
          <option value="JA">Japanese</option>
          <option value="JW">Javanese</option>
          <option value="KO">Korean</option>
          <option value="LA">Latin</option>
          <option value="LV">Latvian</option>
          <option value="LT">Lithuanian</option>
          <option value="MK">Macedonian</option>
          <option value="MS">Malay</option>
          <option value="ML">Malayalam</option>
          <option value="MT">Maltese</option>
          <option value="MI">Maori</option>
          <option value="MR">Marathi</option>
          <option value="MN">Mongolian</option>
          <option value="NE">Nepali</option>
          <option value="NO">Norwegian</option>
          <option value="FA">Persian</option>
          <option value="PL">Polish</option>
          <option value="PT">Portuguese</option>
          <option value="PA">Punjabi</option>
          <option value="QU">Quechua</option>
          <option value="RO">Romanian</option>
          <option value="RU">Russian</option>
          <option value="SM">Samoan</option>
          <option value="SR">Serbian</option>
          <option value="SK">Slovak</option>
          <option value="SL">Slovenian</option>
          <option value="ES">Spanish</option>
          <option value="SW">Swahili</option>
          <option value="SV">Swedish </option>
          <option value="TA">Tamil</option>
          <option value="TT">Tatar</option>
          <option value="TE">Telugu</option>
          <option value="TH">Thai</option>
          <option value="BO">Tibetan</option>
          <option value="TO">Tonga</option>
          <option value="TR">Turkish</option>
          <option value="UK">Ukranian</option>
          <option value="UR">Urdu</option>
          <option value="UZ">Uzbek</option>
          <option value="VI">Vietnamese</option>
          <option value="CY">Welsh</option>
          <option value="XH">Xhosa</option>
        </select>                       
                                        </div>
                                   </div> 

                                   <div class="mainselect_div">
                                    <label class="form-control-label" for="ctlrace_you">Your Ethnicity</label>   
                                        <div class="">  

        <select class=" dropdown" id="ctlrace_you" name="ctlrace_you"  data-plugin="select2">
            <option value="" selected="selected" disabled="disabled">-- select one --</option>
            <optgroup label="White">
              <option value="White English">English</option>
              <option value="White Welsh">Welsh</option>
              <option value="White Scottish">Scottish</option>
              <option value="White Northern Irish">Northern Irish</option>
              <option value="White Irish">Irish</option>
              <option value="White Gypsy or Irish Traveller">Gypsy or Irish Traveller</option>
              <option value="White Other">Any other White background</option>
            </optgroup>
            <optgroup label="Mixed or Multiple ethnic groups">
              <option value="Mixed White and Black Caribbean">White and Black Caribbean</option>
              <option value="Mixed White and Black African">White and Black African</option>
              <option value="Mixed White Other">Any other Mixed or Multiple background</option>
            </optgroup>
            <optgroup label="Asian">
              <option value="Asian Indian">Indian</option>
              <option value="Asian Pakistani">Pakistani</option>
              <option value="Asian Bangladeshi">Bangladeshi</option>
              <option value="Asian Chinese">Chinese</option>
              <option value="Thai, Malay, Filipino">Thai, Malay, Filipino</option>
              <option value="North East Asian">North East Asian (Mongol, Tibetan, Korean Japanese, etc)</option>
              <option value="Asian Other">Any other Asian background</option>
            </optgroup>
            <optgroup label="Black">
              <option value="Black African">African</option>
              <option value="Black African American">African American</option>
              <option value="Black Caribbean">Caribbean</option>
              <option value="Black Other">Any other Black background</option>
            </optgroup>
            <optgroup label="Other ethnic groups">
              <option value="Hispanic">Hispanic</option>
              <option value="Latino">Latino</option>
              <option value="Native American">Native American</option>
              <option value="Indigenous Australian">Indigenous Australian</option>
              <option value="Pacific Islander">Pacific Islander</option>
              <option value="Pacific">Pacific (Polynesian, Micronesian, etc)</option>
              <option value="Arctic">Arctic (Siberian, Eskimo)</option>
              <option value="Arab">Arab</option>
              <option value="Other">Any other ethnic group</option>
            </optgroup>
          </select>

                                        </div>
                                   </div>

                        <div class="mainselect_div"> 
                                <div class="input-group">  
                                <div class="checkbox checkbox-primary" ="">
                                  <input type="checkbox" id="ctlconsent" name="ctlconsent[]" value="1">
                                  <label for="ctlconsent">I  have read, understand and agree to 
                                      <a href="#"> all the Terms And Conditions Listed Here </a></label> 
                                </div> 
                            </div>  
                               </div>                  



                    </div>
                    <div class="col-md-6">

                        <div class="[ info-card ]">
                            <img   src="/img/map.png" alt="Security Seal" />
                            <div class="[ info-card-details ] animate">
                                <div class="[ info-card-header ]">
                                    <h1> Always FREE Delivery  </h1>
                                    <h3> We make sure to deliver fast </h3>
                                </div>
                                <div class="[ info-card-detail ]">
                                    <!-- Description -->
                                    <p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
                                    <div class="social">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 btnrow">




                    <div id="whats-new-submit">
                        <input type="submit" class="btn btn-primary" name="aw-whats-new-submit" id="aw-whats-new-submit" value="<?php esc_attr_e( 'Submit', 'buddypress' ); ?>" />
                    </div>
                        <div id="frmerror"></div>
                        <div id="formsbt">
                            <button type="submit" class="">Submit Report</button>
                        </div>


                    </div>
                </div>

              </div>
            </div>
        </div> 

        <div class="panel panel-default panel-mainform" id="divaccordion_9">
            <div class="panel-heading" role="tab" id="heading1_9">
              <h4 class="panel-title">
                <a id="collapse1_9_btn" class="collapsed collapse_a_heading" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_9" aria-expanded="false" aria-controls="collapse1_9">
                  Payment Details
                </a>
              </h4>
            </div>
            <div id="collapse1_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_9">
              <div class="panel-body">
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-6">   

        <form role="form" id="payment-form" method="POST" action="javascript:void(0);">


                          <div class="input-group">      
                              <input 
                                                    type="tel"
                                                    class="payment-field"
                                                    name="cardNumber" 
                                                     autofocus 
                                                />
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels">Credit Card#</label>
                          </div>   

                          <div class="input-group">      
                             <input name="cardExpiry" type="tel" class="payment-field"   >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels">Expiry (mm/yyyy)</label>
                          </div>       

                          <div class="input-group">      
                           <input name="cardCVC" type="tel" class="payment-field"   >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels">CVC</label>
                          </div>     
            <?php /*
                          <div class="input-group">      
                            <input   type="text"   name="cardholder-name"  >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels">Billing Name</label>
                          </div>

                          <div class="input-group">      
                            <input   type="text"   name="telephone_payment"  >
                            <input   type="tel"   style="display:none;" >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels">Phone</label>
                          </div>     

                          <div class="input-group">      
                            <input   type="text" name="address-zip"  >
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels">Billing Postal Code</label>
                          </div>   
            */ ?>


          <?php /*  
                         <div class="input-group"  style="border-bottom:1px solid #00BFFF;">       
                            <div id="card-element" class="field"></div>  
                            <span class="highlight">Card#</span>
                            <span class="bar"></span>
                            <label class="fancytextinputlabels"></label>
                          </div> 

          <label>
            <span>Name</span>
            <input name="cardholder-name" class="field" placeholder="Jane Doe" />
          </label>
          <label>
            <span>Phone</span>
            <input class="field" placeholder="(123) 456-7890" type="tel" />
          </label>
          <label>
            <span>ZIP code</span>
            <input name="address-zip" class="field" placeholder="94110" />
          </label>
          <label>
            <span>Card</span>
            <div id="card-element" class="field"></div>
          </label>
          */ ?>

                                <div class="row">
                                    <div class="col-xs-12">

                                        <table class="paymenttable table-bordered table-condensed"> 
                                            <tr ><td class="info"><h4>Final Price</h4></td></tr>
                                          <tr> 
                                              <td>$123.23 + $11.23 (HST) = <strong>$131.23</strong></td>
                                          </tr>  
                                            <tr><td  style="font-size:13px;">Delivery is Free and estimated time <br />is between 24-48 hours</td></tr>
                                        </table>

                                    </div>
                                </div>    

                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="subscribe btn btn-success btn-lg btn-block" type="button">Pay $131.23</button>
                                    </div>
                                </div>
                                <div class="row" style="display:none;">
                                    <div class="col-xs-12">
                                        <p class="payment-errors"></p>
                                    </div>
                                </div>  

        </form>

                    </div>     
                    <div class="col-md-6">   

                        <div class="[ info-card ]">
                            <img   src="/img/security_seal.png" alt="Security Seal" />
                            <div class="[ info-card-details ] animate">
                                <div class="[ info-card-header ]">
                                    <h1> Seucure Website  </h1>
                                    <h3> We do not secure your CC info </h3>
                                </div>
                                <div class="[ info-card-detail ]">
                                    <!-- Description -->
                                    <p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
                                    <div class="social"> 
                                    </div>
                                </div>
                            </div>
                        </div>                



                    </div>
                 </div>
                <!-- End Row -->              

              </div>
            </div>
          </div>    
    </div> <!-- end of main accordian --> 
  </div><!-- END col-md-12 --> 
</div>
</form>

<script src="<?php echo get_bloginfo('template_directory'); ?>/assets/js/google_auto_address.js" type="text/javascript"></script>      
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWSUfBhRbJr0WE5wNxtjTmYJSPzPtDxDE&libraries=places&callback=initAutocomplete" async defer></script>
<link rel="stylesheet" href="/wp-content/themes/kleo/assets/form/flipcard.css">  
<link rel="stylesheet" href="/wp-content/themes/kleo/assets/form/custom.css">  
<?php /*<script type="text/javascript" src="/wp-content/themes/kleo/assets/form/jquery.validate.min.js"></script>*/ ?> 
<script type="text/javascript" src="/wp-content/themes/kleo/assets/form/custom.js"></script>