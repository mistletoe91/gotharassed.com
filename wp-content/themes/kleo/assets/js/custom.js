var try_= 0;
jQuery( document ).ready(function() { 
    
    jQuery('#ctlallreports_link').click(function(event) {
         event.preventDefault();
         jQuery( "#reportstab .nav-item .btn-allreports" ).click ()   ;
    });
    
    
    if ( jQuery( ".cssmemberactivity" ).length ) {
        //On Members page hide the form 
        jQuery ("#whats-new-form").hide ();
    }
    

    jQuery(".rtmedia-gallery-item-actions a:first").each(function() {
           // console.log ( jQuery(this).html() );
    });     
    
    
    
    //FlipSide 
    function fnFlipSide (){
        var btn = document.querySelector( '.flipside .btn' );

        var btnFront = btn.querySelector( '.flipside .btn-front' ),
            btnYes = btn.querySelector( '.flipside .btn-back .yes' ),
            btnNo = btn.querySelector( '.flipside .btn-back .no' );

        btnFront.addEventListener( 'click', function( event ) {
          var mx = event.clientX - btn.offsetLeft,
              my = event.clientY - btn.offsetTop;

          var w = btn.offsetWidth,
              h = btn.offsetHeight;

          var directions = [
            { id: 'top', x: w/2, y: 0 },
            { id: 'right', x: w, y: h/2 },
            { id: 'bottom', x: w/2, y: h },
            { id: 'left', x: 0, y: h/2 }
          ];

          directions.sort( function( a, b ) {
            return distance( mx, my, a.x, a.y ) - distance( mx, my, b.x, b.y );
          } );

          btn.setAttribute( 'data-direction', directions.shift().id );
          btn.classList.add( 'is-open' );

        } );

        btnYes.addEventListener( 'click', function( event ) {	
          btn.classList.remove( 'is-open' );
        } );

        btnNo.addEventListener( 'click', function( event ) {
          btn.classList.remove( 'is-open' );
        } );
        
        jQuery ('#loadingli').hide ();
        
        btnFront. click ();
 
    }//end function
    
    function distance( x1, y1, x2, y2 ) {
      var dx = x1-x2;
      var dy = y1-y2;
      return Math.sqrt( dx*dx + dy*dy );
    }        
    
    //Homepage Effect  
    if ( jQuery( "#homepage_active_report" ).length && true) {
        
        

        //Timeline beautify 
        jQuery(".activity-time-since").each(function() {
           var eleDest = jQuery(this).parent().parent().parent(); 
           jQuery(this) .detach().appendTo ( eleDest );
        }); 
        
        //First reverse and set the  childern
		jQuery('#homepage_active_report #activity-stream')[0].scrollTop = jQuery('#homepage_active_report #activity-stream')[0].scrollHeight; 
  
        //now go one by one and delete the childern
            var $delay_ = 2000; //seconds
            var $i_ = 1;  		
            var $totalKids_ = jQuery("#homepage_active_report #activity-stream li").length;
            jQuery(jQuery("#homepage_active_report #activity-stream").children().get().reverse()).each(function() { 
                 //Uncomment following to restrict deletion of few childern
                 /*if(($i_+3)>$totalKids_){return ;}*/    
                var targetId_ = jQuery(this).attr('id');               
                var fun_ = function(targetId_) {  deleteLastElement_ForReal(targetId_); }
                setTimeout(fun_, $i_++*$delay_ , targetId_);
            });  
            setTimeout(function() {  showFlipSide(); }, $i_++*$delay_);
        
    }//endif
    
    function showFlipSide (){
        

        var $flipSide = '<div class="flipside"><div class="btn"><div class="btn-back"><h5 class="headingmy">Get your voice heard</h5><p><a href="/reports/#link_new" class="btn-red">Report Harassment Now !</a></p><button class="yes hide">Yes</button><button class="no hide">No</button>  </div>  <div class="btn-front"><i  id="loadingli" class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div></div></div>';
        
            jQuery('#homepage_active_report #activity-stream').append ('<li>'+$flipSide+'</li>').hide().fadeIn(0); 
            setTimeout(function() {  fnFlipSide(); }, 1000);
        
    }//end function 
     
    
    function mydatevalidation_ (dis){
        
         var d = new Date();
         var n = d.getFullYear();
        
         var month_ = 0;
         var day_ = 0;
         var year_ = 0;
         val = jQuery(dis).val().split("/"); 
         if(val.length == 1){
             //Month
             if(parseInt(val[0]) > 12 ){
                 val[0]= 12;
             }//endif   
             jQuery(dis).val(val[0]) ;
         } else {
           if(val.length == 2)  { 
              
             if(parseInt(val[0],10) == 2 ){
                 //feb month 
                 if(parseInt(val[1]) > 29){
                     val[1]= 29;
                 }//endif                     
             } else { 
                 if(parseInt(val[1]) > 31){
                     val[1]= 31;
                 }//endif                     
             }//endif                
               
             jQuery(dis).val(val[0]+'/'+val[1]) ;
           } else {
               if(val.length ==3 ){
                 
             
             if(parseInt(val[0],10) == 2 ){
                 //feb month 
                 if(parseInt(val[1]) > 29){
                     val[1]= 29;
                 }//endif                     
             } else { 
                 if(parseInt(val[1]) > 31){
                     val[1]= 31;
                 }//endif                     
             }//endif 
                   
                   if(parseInt(val[2]) >=1920 && parseInt(val[2]) <= n ){
                   } else {
                       if(parseInt(val[2],10) <= 999){
                           //user is still typing in
                       } else {
                           if(parseInt(val[2])>0){
                            val[2] = n;    
                           }                           
                       } 
                   }
                   jQuery(dis).val(val[0]+'/'+val[1]+'/'+val[2]) ;
               }//endif
           }
         }         
    }//end function 
    
    if ( jQuery( "#whats-new-form #ctldate" ).length && true) {
        
        jQuery('#whats-new-form #ctldate').mask('00/00/0000',{placeholder:"MM/DD/YYYY"});   
        jQuery("#whats-new-form #ctldate").keypress(function(){
            mydatevalidation_ (this); 
        });
        jQuery("#whats-new-form #ctldate").blur(function(){
            mydatevalidation_ (this); 
        });  
        
        //prevent google auto compltete address to submint form when user hit enter
        jQuery('#txtincident_location').keydown(function (e) { 
          if (e.which == 13 && jQuery('.pac-container:visible').length) return false;
        });
    }//endif 
   
    jQuery('#formsbt .btn-primary').click(function(e){   
        try_++;        
        if(try_>1){            
            return false;
        } 
          
        e.preventDefault();        
        jQuery('#frmerror').html ('');
        
        error_ = '';
        if (! jQuery('#ctlconsent')[0].checked){
           error_ = 'Your consent to terms and conditions is required. Please review and agree. ';
        }
 
        if (!jQuery('#whats-new').val()) { 
            error_ = 'Incident Details is a required field. Please fill it above and try again'; 
        } 
        
        if(error_.length>0){
            error_ ='<div class="alert alert-danger" role="alert">'+error_+'</div>';
            jQuery('#frmerror').html (error_);
            try_ = 0;
            return false;
        }//endif 
         
        info_ = '<div class="alert alert-info" role="alert"><i style="font-size:1.2em" class="fa fa-cog fa-spin  fa-fw"></i><span class="sr-only">Loading...</span>Loading... Please wait !</div>';
        jQuery('#formsbt').html (info_);  
        
        //now click the button
        jQuery('#aw-whats-new-submit').click ();
        

    });      
    
    /*
    jQuery('#whats-new-form123').submit(function(e){   
        try_++;        
        if(try_>1){            
            return false;
        } 
          
        //e.preventDefault();
        jQuery('#frmerror').html ('');
        
        error_ = '';
        if (! jQuery('#ctlconsent')[0].checked){
           error_ = 'Your consent to terms and conditions is required. Please review and agree. ';
        }

        console.log (jQuery('#whats-new').val());
        if (!jQuery('#whats-new').val()) { 
            error_ = 'Incident Details is a required field. Please fill it above and try again'; 
        } 
        
        if(error_.length>0){
            error_ ='<div class="alert alert-danger" role="alert">'+error_+'</div>';
            jQuery('#frmerror').html (error_);
            try_ = 0;
            return false;
        }//endif 
        info_ = '<div class="alert alert-info" role="alert"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>Loading... Please wait !</div>';
        jQuery('#formsbt').html (info_); 
    });  
    */
    
    //functions 
    function deleteLastElement_ForReal (targetId_){ 
			jQuery("#"+targetId_).hide('fast', function(){  jQuery("#"+targetId_).remove(); });
	}     
      
     
});

 
