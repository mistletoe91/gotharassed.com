jQuery( document ).ready(function() {
    
    var d = new Date();
    var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() +"-" + d.getHours() + "-" + d.getMinutes();

    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };  
    var color = Chart.helpers.color;
    
    function marklabels() { 
                          var ctx = this.chart.ctx;
                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'bottom';

                          this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                              var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                  total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                  mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                  start_angle = model.startAngle,
                                  end_angle = model.endAngle,
                                  mid_angle = start_angle + (end_angle - start_angle)/2;

                              var x = mid_radius * Math.cos(mid_angle);
                              var y = mid_radius * Math.sin(mid_angle);

                              ctx.fillStyle = '#fff';
                              if (i == 3){ // Darker text color for lighter background
                                ctx.fillStyle = '#444';
                              }
                              var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                              ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                              // Display percent in another line, line break doesn't work for fillText
                              ctx.fillText(percent, model.x + x, model.y + y + 15);
                            }
                          });   
   } //endif   
    
    
    //Main 4
    var config1 = {
            type: 'doughnut',
            responsive: true,
            data: {
                datasets: [{
                    data: [ 
                    ],
                    backgroundColor: [ 
                    ],
                    label: 'Dataset 1'
                }],
                labels: [ 
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                    ,onComplete: marklabels                
                }
                


            }
    };
    
    var config2 = {
            type: 'pie',
            responsive: true,
            data: {
                datasets: [{
                    data: [ 
                    ],
                    backgroundColor: [ 
                    ],
                    label: 'Dataset 1'
                }],
                labels: [ 
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                    ,onComplete: marklabels 
                }
            }
        }; 
    var config3 = {
            type: 'pie',
            responsive: true,
            data: {
                datasets: [{
                    data: [ 
                    ],
                    backgroundColor: [ 
                    ],
                    label: 'Dataset 1'
                }],
                labels: [ 
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                    ,onComplete: marklabels 
                }
            }
        }; 
    var config4 = {
            type: 'doughnut',
            responsive: true,
            data: {
                datasets: [{
                    data: [ 
                    ],
                    backgroundColor: [ 
                    ],
                    label: 'Dataset 1'
                }],
                labels: [ 
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                    ,onComplete: marklabels 
                }
            }
        };    
    
    
    //and Harassement Type
    var config_o_type = {
        type: 'radar',
        data: {
            labels: [],
            datasets: [{
                label: "Number of harassment incidents",
                backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
                borderColor: window.chartColors.red,
                pointBackgroundColor: window.chartColors.red,
                data: [ 
                ]
            }]
        },
        options: { 
            responsive: true,
            legend: {
                position: 'top',
            },             
            scale: {
              ticks: {
                beginAtZero: true
              }
            }
        }
    };  
             
    
    
    
    var config_h_type = {
        data: {
            datasets: [{
                data: [ 
                ],
                backgroundColor: [ 
                ],
                label: 'My dataset' // for legend
            }],
            labels: [ 
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top',
            }, 
            scale: {
              ticks: {
                beginAtZero: true
              },
              reverse: false
            },
            animation: {
                animateRotate: false,
                animateScale: true 
            }
        }
    };     
    
       
    
    var config_number_of = {
        labels: [],
        datasets: [{
            label: "Number of Offenders Per Incident",
            borderColor: window.chartColors.blue,
            backgroundColor: window.chartColors.blue,
            fill: false,
            data: [
                 
            ],
            yAxisID: "y-axis-2"
        }]
    }; 
    
    var config_number_of_s = {
            type: 'pie',
            responsive: true,
            data: {
                datasets: [{
                    data: [ 
                    ],
                    backgroundColor: [ 
                    ],
                    label: 'Number of Offenders Per Incident'
                }],
                labels: [ 
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };     
    
    var config_source = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: 'Dataset 1',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ]
            }, {
                label: 'Dataset 2',
                backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                borderColor: window.chartColors.blue,
                borderWidth: 1,
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ]
            }]

   };       
    
    var config_source_s = {
            type: 'pie',
            responsive: true,
            data: {
                datasets: [{
                    data: [ 
                    ],
                    backgroundColor: [ 
                    ],
                    label: 'Number of Offenders Per Incident'
                }],
                labels: [ 
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };       
       
 
   var config_location_vs_sev = {
			type: 'line',
			data: {
				labels: [],
				datasets: [{
					label: "Sev1",
					borderColor: window.chartColors.red,
					backgroundColor: window.chartColors.red,
					data: [ 
                    ],
				}, {
					label: "Sev2",
					borderColor: window.chartColors.blue,
					backgroundColor: window.chartColors.blue,
					data: [ 
                    ],
				}, {
					label: "Sev3",
					borderColor: window.chartColors.green,
					backgroundColor: window.chartColors.green,
					data: [ 
                    ],
				}, {
					label: "Sev4",
					borderColor: window.chartColors.yellow,
					backgroundColor: window.chartColors.yellow,
					data: [ 
                    ],
				}]
			},
			options: {
				responsive: true, 
				tooltips: {
					mode: 'index',
				},
				hover: {
					mode: 'index'
				},
				scales: {
					xAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Countries'
						}
					}],
					yAxes: [{
						stacked: true,
						scaleLabel: {
							display: true,
							labelString: '%age of reports'
						}
					}]
				}
			}
		};       
    
    //Get Data :  
    jQuery.getJSON( "/scripts/cron_stats.json", { datetimestamp_for_no_caching : strDate } )
      .done(function( json ) {
         
          //Get Data One By One and Make Graph           
          var colors_ = ["red","blue","yellow","green","orange","purple","grey"];
        
          var counter_ = 0;    
          for (var i in json.data.ctlgender) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctlgender [i]; 
                config1.data.datasets [0].data.push ( item );
                config1.data.datasets [0].backgroundColor.push ( window.chartColors [ colors_[counter_++] ]); 
                config1.data.labels.push (i); 
          }//end for  
        
          var counter_ = 0;    
          for (var i in json.data.ctlagegroup) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctlagegroup [i]; 
                config2.data.datasets [0].data.push ( item );
                config2.data.datasets [0].backgroundColor.push ( window.chartColors [ colors_[counter_++] ]); 
                config2.data.labels.push (i); 
          }//end for         
        
          var counter_ = 0;     
          for (var i in json.data.ctlfactors_physical_1) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctlfactors_physical_1 [i]; 
                config3.data.datasets [0].data.push ( item );
                config3.data.datasets [0].backgroundColor.push ( window.chartColors [ colors_[counter_++] ]); 
                config3.data.labels.push (i); 
          }//end for        
        
          var counter_ = 0;    
          for (var i in json.data.ctlsev) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctlsev [i]; 
                config4.data.datasets [0].data.push ( item );
                config4.data.datasets [0].backgroundColor.push ( window.chartColors [ colors_[counter_++] ]); 
                config4.data.labels.push (i); 
          }//end for       
        
          //Incident Type
          var counter_ = 0;    
          for (var i in json.data.ctltype) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctltype [i]; 
                if(item.length<=0){
                    continue;
                }//endif

                config_o_type.data.datasets [0].data.push ( item ); 
                config_o_type.data.labels.push (i);               
          }//end for   
        
          //Incident Type
          var counter_ = 0;    
          for (var i in json.data.ctltypeofoffender) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctltypeofoffender [i];  
                config_h_type.data.datasets [0].data.push ( item ); 
                config_h_type.data.labels.push (i);  
                config_h_type.data.datasets [0].backgroundColor.push ( color(window.chartColors[ colors_[counter_++]]).alpha(0.5).rgbString() ); 
          }//end for          
        
          //Number of Offenders
          var counter_ = 0;    
          for (var i in json.data.ctlnumberofoffenders) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctlnumberofoffenders [i];  
                config_number_of.datasets [0].data.push ( item ); 
                config_number_of.labels.push (i);   
          }//end for         
        
          //Number of Offenders - Small Devices
          var counter_ = 0;    
          for (var i in json.data.ctlnumberofoffenders) {   
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.ctlnumberofoffenders [i];  
                config_number_of_s.data.datasets [0].data.push ( item ); 
                config_number_of_s.data.labels.push (i);  
                config_number_of_s.data.datasets [0].backgroundColor.push ( window.chartColors [ colors_[counter_++] ]);                  
          }//end for   
         

          //Location vs sev
          var counter_ = 0;    
          var counterCountries_ = 0;  
          for (var i in json.data.loc_sev) {
                if(counter_>=(colors_.length-1)){
                    counter_ = 0;
                }
                var item = json.data.loc_sev [i];
                config_location_vs_sev.data.labels.push (i);   
                
                for (var ii in item) {   
                       var vv = item [ii];        
                       config_location_vs_sev.data.datasets [(ii-1)].data [counterCountries_] = vv;
                }//end for
                counterCountries_++;
          }//end for   
        
            //Now Apply  
    var myChart1 = new Chart(document.getElementById("chart1").getContext("2d"), config1); 
    var myChart2 = new Chart(document.getElementById("chart2").getContext("2d"), config2); 
    var myChart3 = new Chart(document.getElementById("chart3").getContext("2d"), config3); 
    var myChart4 = new Chart(document.getElementById("chart4").getContext("2d"), config4);     
    
    var myChart5 = new Chart(document.getElementById("chart5").getContext("2d"), config_o_type);     
    var myChart6 = new Chart.PolarArea(document.getElementById("chart6").getContext("2d"), config_h_type);     
     
        /*
    var myChart7 = Chart.Line(document.getElementById("chart7").getContext("2d"), {
            data: config_number_of,
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false, 
                scales: {
                    yAxes: [{
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "left",
                        id: "y-axis-1",
                    }, {
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "right",
                        id: "y-axis-2",

                        // grid line settings
                        gridLines: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    }],
                }
            }
     });       
    var myChart8 =  new Chart(document.getElementById("chart8").getContext("2d"), {
                type: 'bar',
                data: config_source,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    } 
                }
     });    
    
    var myChart7_s = new Chart(document.getElementById("chart7_s").getContext("2d"), config_number_of_s); 
    var myChart8_s = new Chart(document.getElementById("chart8_s").getContext("2d"), config_source_s);     
    */
    var myChart9 = new Chart(document.getElementById("chart9").getContext("2d"), config_location_vs_sev);     
          
    jQuery('#ctlstats').fadeIn ();
    jQuery('.ctlstats_loading').fadeOut ();
         
        
        jQuery ('#timestampright').html ('Last Updated : '+ json.time + " UTC");
        
      })
      .fail(function( jqxhr, textStatus, error ) {
        //var err = textStatus + ", " + error;
        //console.log( "Request Failed: " + err );
    });    

    
     

    
});