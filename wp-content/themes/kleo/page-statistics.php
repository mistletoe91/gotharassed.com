<?php  
get_header();  
/* #<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/assets/chart/chart.css';?>"> */
?>  
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/form/flipcard.css">  

<section class="container-wrap main-color">
	<div id="main-container" class="container">
		<div class="row"> 
			<div class="template-page col-sm-12 tpl-no">
				<div class="wrap-content"> 
<!-- Begin Article -->
<article id="post-68" class="clearfix post-68 page type-page status-publish"> 
	<div class="article-content graphspage" id="whats-new-form">
  
      <!-- Page Content Start -->
        
    <!-- Start Graphs -->         
    <div class="container-fluid">    
        
<div class="row" >
  <div class="starter-template">
      <h1 class="page-header">Quick Look at <span> the statistics </span></h1>
        <p class="lead">This data is generated as an aggregate based on the submitted harassment reports. Values are in percentage. <span id="timestampright" class=" "></span></p>
  </div>
</div>     
    </div>
    <!-- End Graphs -->
 
        
<div class="ctlstats_loading"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
<span class="sr-only">Loading...</span></div>
        
<div   id="ctlstats">
        
    
   
    
        <!-- START -->
        <div class="container-fluid">
                  <div class="row"> 
                       <div class="col-xs-12 col-sm-6 col-md-3">     
                            <canvas id="chart1" width="300" height="400"></canvas>
                          </div>                              
                      <div class="col-xs-12 col-sm-6 col-md-3"> 
                           <div class="[ info-card ]"> 
                                <div class="bigicon symbolstats">&#9893;</div>
                                <div class="[ info-card-details ] animate">
                                    <div class="[ info-card-header ]">
                                        <h1> Victims  </h1>
                                        <h3>By Gender</h3>
                                    </div>
                                    <div class="[ info-card-detail ]">
                                        <!-- Description -->
                                        <p>Represent percentage of victims who identify themselves as male, female or other</p>
                                        <div class="social"> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                       <div class="col-xs-12 col-sm-6 col-md-3">  
                            <canvas id="chart2" width="300" height="400"></canvas>
                          </div>                     
                       <div class="col-xs-12 col-sm-6 col-md-3">   
                           <div class="[ info-card ]"> 
                                <div class="bigicon symbolstats">&#x231B;</div>
                                <div class="[ info-card-details ] animate">
                                    <div class="[ info-card-header ]">
                                        <h1> Victims </h1>
                                        <h3>By Age</h3>
                                    </div>
                                    <div class="[ info-card-detail ]">
                                        <!-- Description -->
                                        <p>Represent percentage of victims who identify themselves under corresponding age group</p>
                                        <div class="social"> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                  </div>
            
                  <div class="row"> 
                       <div class="col-xs-12 col-sm-6 col-md-3">     
                            <canvas id="chart3" width="300" height="400"></canvas>
                          </div>                              
                      <div class="col-xs-12 col-sm-6 col-md-3"> 
                           <div class="[ info-card ]"> 
                                <div class="bigicon symbolstats">&#x270A;</div>
                                <div class="[ info-card-details ] animate">
                                    <div class="[ info-card-header ]">
                                        <h1> Victim   </h1>
                                        <h3>Harassement involve physical attack </h3>
                                    </div>
                                    <div class="[ info-card-detail ]">
                                        <!-- Description -->
                                        <p>Physical attacks are common in certain types of harassment. Graph displays aggregate number of such incidents</p>
                                        <div class="social"> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                       <div class="col-xs-12 col-sm-6 col-md-3">  
                            <canvas id="chart4" width="300" height="400"></canvas>
                          </div>                     
                       <div class="col-xs-12 col-sm-6 col-md-3">   
                           <div class="[ info-card ]"> 
                                <div class="bigicon symbolstats">&#x2690;</div>
                                <div class="[ info-card-details ] animate">
                                    <div class="[ info-card-header ]">
                                        <h1> Severity Level</h1>
                                        <h3>What is the severity level of incident</h3>
                                    </div>
                                    <div class="[ info-card-detail ]">
                                        <!-- Description -->
                                        <p>Severity level helps organizing the urgency of incident</p>
                                        <div class="social"> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                  </div>         
            
                  <div class="row"> 
                       <div class="col-xs-12 col-sm-6 col-md-6">     
                            <canvas id="chart5" width="300" height="400"></canvas>
                          </div>                              
                      <div class="col-xs-12 col-sm-6 col-md-6"> 
                           <div class="[ info-card singlegraph ]"> 
                                <i class="bigicon fa fa-tags" aria-hidden="true"></i>
                                <div class="[ info-card-details ] animate">
                                    <div class="[ info-card-header ]">
                                        <h1> Incident Type   </h1>
                                        <h3>Category of harassment</h3>
                                    </div>
                                    <div class="[ info-card-detail ]">
                                        <!-- Description -->
                                        <p>Victim can report more than one type of harassment. It helps categorizing incidents</p>
                                        <div class="social"> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div>
                    <div class="row"> 
                       <div class="col-xs-12 col-sm-6 col-md-6">  
                            <canvas id="chart6" width="300" height="400"></canvas>
                        </div>                     
                       <div class="col-xs-12 col-sm-6 col-md-6">   
                           <div class="[ info-card ] singlegraph"> 
                                <i class="bigicon fa fa-optin-monster" aria-hidden="true"></i>
                                <div class="[ info-card-details ] animate">
                                    <div class="[ info-card-header ]">
                                        <h1>Offender Type </h1>
                                        <h3>Who is the offender</h3>
                                    </div>
                                    <div class="[ info-card-detail ]">
                                        <!-- Description -->
                                        <p>Offender can be co-worker, employer, ex-partner etc. Graph describe aggregate of reported offender types</p>
                                        <div class="social"> 
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                  </div>              
        </div>
        <!-- END --> 
         
         
        <!-- Large Devices   -->   
        <div class="container-fluid somepadding hidden-xs">
          <div class="row blackrow">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label>Top Reported Countries vs Harasement Severity</label>
                <canvas id="chart9" width="600" height="200"></canvas>
            </div>            
          </div>
        </div>
        
      <!-- Page Content End -->
    
</div>    
        
        
        <?php /*
        <!-- Small & Medium   --> 
        <div class="hidden-sm hidden-md hidden-lg ">
            <div class="container-fluid ">
              <div class="row blackrow">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <label>Offenders per Incident </label>
                    <canvas id="chart7_s" width="300" height="400"></canvas>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <div class="row blackrow">        
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <label>Victims per Incident</label>
                    <canvas id="chart8_s" width="300" height="400"></canvas>
                </div>
              </div>
            </div>            
        </div>

        
        <!-- Large Devices   -->   
        <div class=" hidden-xs  ">
            <div class="container-fluid">
              <div class="row blackrow">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <label>Offenders per Incident  </label>
                    <canvas id="chart7" class="barlinechart" width="600" height="200"></canvas>
                </div>            
              </div>
            </div>  
            <div class="container-fluid">
              <div class="row blackrow">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <label>Victims per Incident </label>
                    <canvas id="chart8"  class="barlinechart" width="600" height="200"></canvas>
                </div>            
              </div>
            </div>                
        </div>
        
        
          
        */ ?>
        
        
	</div><!--end article-content--> 
</article>
<!-- End  Article --> 
				</div><!--end wrap-content-->
			</div><!--end main-page-template-->
						</div><!--end .row-->		</div><!--end .container--> 
</section> 
  
<script src="<?php echo get_template_directory_uri() . '/assets/chart/chart.bundle.min.js';?>"></script>
<script src="<?php echo get_template_directory_uri() . '/assets/chart/utils.js';?>"></script>


<script src="<?php echo get_template_directory_uri() . '/assets/chart/chart.custom.js';?>"></script>

<?php  
/*
<script src="<?php echo get_template_directory_uri() . '/assets/chart/markerclusterer.js';?>"></script> 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWSUfBhRbJr0WE5wNxtjTmYJSPzPtDxDE&callback=initMap"></script>
*/
get_footer();  
?> 